import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import {IonicApp, IonicErrorHandler, IonicModule} from 'ionic-angular';
import { SplashScreen } from '@ionic-native/splash-screen';
import { StatusBar } from '@ionic-native/status-bar';

import { MyApp } from './app.component';
import {LoginPage} from "../pages/login/login";
import {RegistrationPage} from "../pages/registration/registration";
import {SettingsPage} from "../pages/settings/settings";
import {HttpClientModule} from "@angular/common/http";
import {AuthService} from "../services/auth";
import {TestPage} from "../pages/test/test";
import {ForgotVerifyPage} from "../pages/forgot/forgot-verify/forgot-verify";
import {ForgotCodePage} from "../pages/forgot/forgot-code/forgot-code";
import {ForgotPasswordsPage} from "../pages/forgot/forgot-passwords/forgot-passwords";
import {TabsPage} from "../pages/tabs/tabs";
import {DashboardPage} from "../pages/dashboard/dashboard";
import {ReportsPage} from "../pages/reports/reports";
import {Point2pointPage} from "../pages/point2point/point2point";
import {MirouteHeaderPage} from "../pages/miroute-header/miroute-header";
import {MirouteFooterPage} from "../pages/miroute-footer/miroute-footer";
import {AgmCoreModule} from "@agm/core";
import {Geolocation} from "@ionic-native/geolocation";
import {IonicStorageModule} from "@ionic/storage";
import {MileageEntryPage} from "../pages/mileage-entry/mileage-entry";
import {MirouteStartPage} from "../pages/miroute/miroute-start/miroute-start";
import {MirouteDrivingPage} from "../pages/miroute/miroute-driving/miroute-driving";
import {MirouteStoppedPage} from "../pages/miroute/miroute-stopped/miroute-stopped";
import {MirouteReviewPage} from "../pages/miroute/miroute-review/miroute-review";
import {MirouteEndPage} from "../pages/miroute/miroute-end/miroute-end";
import {MirouteNewStopPage} from "../pages/miroute/miroute-new-stop/miroute-new-stop";
import {ApiService} from "../services/api";
import {StorageService} from "../services/storage";
import {EditTripModalPage} from "../pages/partials/edit-trip-modal/edit-trip-modal";
import {ComponentsModule} from "../components/components.module";
import {CallNumber} from "@ionic-native/call-number";
import {InfoPage} from "../pages/info/info";
import {Keyboard} from "@ionic-native/keyboard";
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";
import {MirouteDayEndedPage} from "../pages/miroute/miroute-day-ended/miroute-day-ended";

@NgModule({
  declarations: [
    MyApp,
    LoginPage,
    InfoPage,
    ForgotVerifyPage,
    ForgotCodePage,
    ForgotPasswordsPage,
    RegistrationPage,
    SettingsPage,
    TabsPage,
    DashboardPage,
    ReportsPage,
    Point2pointPage,
    MileageEntryPage,
    MirouteStartPage,
    MirouteDrivingPage,
    MirouteStoppedPage,
    MirouteReviewPage,
    MirouteEndPage,
    MirouteDayEndedPage,
    MirouteNewStopPage,
    MirouteReviewPage,
    MirouteHeaderPage,
    MirouteFooterPage,
    EditTripModalPage,
    TestPage
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    ComponentsModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot(),
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow'
    })
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    LoginPage,
    InfoPage,
    ForgotVerifyPage,
    ForgotCodePage,
    ForgotPasswordsPage,
    RegistrationPage,
    SettingsPage,
    TabsPage,
    DashboardPage,
    ReportsPage,
    Point2pointPage,
    MileageEntryPage,
    MirouteStartPage,
    MirouteDrivingPage,
    MirouteStoppedPage,
    MirouteReviewPage,
    MirouteEndPage,
    MirouteDayEndedPage,
    MirouteNewStopPage,
    MirouteReviewPage,
    MirouteHeaderPage,
    MirouteFooterPage,
    EditTripModalPage,
    TestPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    ApiService,
    AuthService,
    Geolocation,
    StorageService,
    CallNumber,
    Keyboard,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
