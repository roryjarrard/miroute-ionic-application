import {Component, ViewChild} from '@angular/core';
import {MenuController, NavController, Platform, Tabs} from 'ionic-angular';
import {StatusBar} from '@ionic-native/status-bar';
import {SplashScreen} from '@ionic-native/splash-screen';
import {LoginPage} from "../pages/login/login";
import {SettingsPage} from "../pages/settings/settings";
import {AuthService} from "../services/auth";
import {TestPage} from "../pages/test/test";
import {TabsPage} from "../pages/tabs/tabs";
import {MirouteEndPage} from "../pages/miroute/miroute-end/miroute-end";
import {InfoPage} from "../pages/info/info";


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  rootPage: any;
  settingsPage = SettingsPage;
  infoPage = InfoPage;
  loginPage = LoginPage;
  testPage = TestPage;
  tabsPage = TabsPage;
  isAuthenticated: boolean = false;

  @ViewChild('nav') nav: NavController;
  @ViewChild('mirouteTabs') tabRef: Tabs;

  constructor(platform: Platform,
              statusBar: StatusBar,
              splashScreen: SplashScreen,
              private menuCtrl: MenuController,
              private authService: AuthService) {
    this.authService.endDay.subscribe(
      endDay => {
        if (endDay) {
          this.nav.push(MirouteEndPage);
        }
      }
    );

    this.authService.authUpdated.subscribe(
      auth => {
        this.isAuthenticated = auth;
        console.log('app: authUpdated', auth);
        if (auth) {
          this.nav.setRoot(TabsPage);
        } else {
          this.nav.setRoot(LoginPage);
        }
      }
    );

    platform.ready().then(() => {
      console.log('app: platform is ready');
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      statusBar.styleDefault();
      splashScreen.hide();

     /* if (platform.is('mobileweb') && !platform.is('cordova')) {
        this.authService.resyncData();
      }*/

      /*this.authService.endDay.subscribe(
        endDay => {
          if (endDay) {
            this.nav.push(MirouteEndPage);
          }
        }
      );

      this.authService.authUpdated.subscribe(
        auth => {
          this.isAuthenticated = auth;
          console.log('app: authUpdated', auth);
          if (auth) {
            this.nav.setRoot(TabsPage);
          } else {
            this.nav.setRoot(LoginPage);
          }
        }
      );*/
    });

    if (platform.is('cordova')) {
      /*platform.pause.subscribe(() => {

      });*/

      platform.resume.subscribe(() => {
        this.authService.resyncData();
      });
    }


    // this will force to go look at server
    //console.log('app: update auth to null');
    // this.authService.fetchUserInfo()
    //   .then((userInfo: UserInfo) => {
    //     console.log('app: got new user info', userInfo);
    //   });
  }

  onLoad(page: any) {
    console.log('app: onLoad with page ' + page);
    if (page == this.settingsPage || page == this.infoPage) {
      this.nav.push(page);
    } else {
      this.nav.setRoot(page);
    }
    this.menuCtrl.close();
  }

  onLogout() {
    this.authService.logout()
      .then(() => {
        this.isAuthenticated = false;
        this.menuCtrl.close();
        this.authService.updateAuth(false);
        this.nav.setRoot(LoginPage);
      })
      .catch(err => {
        this.isAuthenticated = false;
        this.menuCtrl.close();
        this.authService.updateAuth(false);
        this.nav.setRoot(LoginPage);
      });
  }
}

