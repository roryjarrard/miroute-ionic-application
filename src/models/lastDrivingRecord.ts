export interface LastDrivingRecord {
  id?: number;
  user_id?: number;

  trip_date?: string;
  last_state?: string;

  destination?: string;
  business_purpose?: string;

  starting_odometer?: number;
  ending_odometer?: number;

  business_mileage?: number;
  personal_mileage?: number;
  commuter_mileage?: number;
  gap_mileage?: number;

  is_ended?: boolean;
  is_today?: boolean;

  created_at?: string;
  updated_at?: string;
}
