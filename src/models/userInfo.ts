import {Trip} from "./trip";
import {Coordinator} from "./coordinator";
import {LastDrivingRecord} from "./lastDrivingRecord";

export interface UserInfo {
  access_token?: string;
  cardataController?: Coordinator;
  companyController?: Coordinator;
  company_id?: number;
  company_name?: string;
  country: string;
  mileage_label: string;
  driverStatusItems?: Object[];
  email?: string;
  first_name?: string;
  is_demo_driver?: boolean;
  lastDrivingRecord?: LastDrivingRecord;
  last_ending_odometer?: number;
  last_entry_date?: string;
  last_name?: string;
  last_starting_odometer?: number;
  last_stop_name?: string;
  lastStop?: Trip;
  mileage_entry_method?: string;
  miroute_page?: string;
  mirouteCoordinator?: Coordinator;
  serverTime?: string;
  street_light_color?: string;
  street_light_id?: number;
  trips?: Trip[];
  use_stop_contract?: number;
  use_tickets?: number;
  user_id?: number;
  username?: string;
  userTime?: string;
}
