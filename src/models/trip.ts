import {Stop} from "./stop";

export interface Trip {
  id?: number;
  created_at?: string;
  event?: string;
  business_purpose?: string;
  mileage_adjusted?: number;
  mileage_original?: number;
  mileage_comment?: string;
  sequence_number?: number;
  trip_date: string;
  time?: string; // converted from created_at server-side
  stop?: Stop;
}
