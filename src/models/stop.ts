export interface Stop {
  id?: number;
  distance?: number;
  city?: string;
  company_id?: number;
  company_stop_id?: number;
  country?: string;  // converted from country_id server-side
  country_id?: number;
  created_at?: string;
  driver_stop_id?: number;
  latitude?: number;
  longitude?: number;
  name?: string;
  state?: string; // converted from state_province_id server-side
  state_province_id?: number;
  street?: string;
  street2?: string;
  type?: string;
  zip_postal?: string;
}
