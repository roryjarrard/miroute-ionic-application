import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Injectable} from "@angular/core";

import moment from 'moment';
import {StorageService} from "./storage";

import {environment as ENV} from "../environments/environment";

@Injectable()
export class ApiService {
  private readonly api_domain: string;
  private access_token: string = '';

  constructor(private http: HttpClient,
              private storageService: StorageService) {
    console.log('api: constructor BASE_URL ' + ENV.BASE_URL);
    this.api_domain = ENV.BASE_URL;
    this.checkAccessToken()
      .then(() => {
        // at this point we should have client secret and access token (or not)
        //console.log('api: access token in storage');
      })
      .catch(err => {
        console.log('Unable to obtain access token', err);
      });
  }

  static getUserTime() {
    return moment().format('YYYY-MM-DD HH:mm:ss');
  }

  getPostHeaders() {
    return {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Bearer ' + this.access_token
      })
    }
  }

  /**
   * Check personal access token is saved locally, and still valid on server.
   * A valid access token means the user is logged in.
   */
  checkAccessToken() {
    console.log('api: checkAccessToken ...');
    return new Promise<any>((resolve, reject) => {

      this.storageService.getUserInfo('access_token')
        .then((access_token: string) => {
          console.log('api: checkAccessToken', access_token);
          if (access_token != null && access_token != '') {
            // we have an access token in storage
            this.access_token = access_token;
            resolve(access_token);
          } else {
            reject('There is no access token');
          }
        })
        .catch(err => {
          console.log('api: checkAccessToken fail', err);
          reject(err);
        });
    });
  }

  /**
   * Auth service will have a new access token after login, set it here
   */
  setAccessToken(token: string) {
    this.access_token = token;
    return this.storageService.set('access_token', token);
  }

  /**
   * Check server that access token is still valid (24 hour lifecycle).
   *
   * @returns {Observable<Object>}
   */
  validateToken() {
    return new Promise<any>((resolve, reject) => {
      this.checkAccessToken()
        .then(data => {
          // we have a token, do we have a username?
          this.storageService.getUserInfo('username')
            .then((username: string) => {
              return this.fetch('validateToken', {token: data, username: username}, false)
                .subscribe(
                  data => {
                    console.log("In validateToken", data);
                    resolve(data)
                  },
                  err => {
                    console.log("In validateToken", err);
                    resolve(err)
                  }
                );
            })
            .catch(err => {
              // we have no username, so we can't validate our token
              console.log('api: cannot validate token, no username');
              reject(err);
            });
        })
        .catch(err => {
          console.log('api: failed to checkAccessToken', err);
          reject(err);
        });
    });
  }

  /**
   * Generic call to server
   * Note: method should usually be post so we can include userTime
   *
   * @param {string} func: route on api
   * @param body: data on pose
   * @param {boolean} authorized: if not auth, use pre header, else post headers
   */
  fetch(func: string, body: object, authorized: boolean) {
    body['function'] = func;
    body['userTime'] = ApiService.getUserTime();

    const obj = JSON.stringify(body);

    let headers = this.getPostHeaders();

    return this.http.post(this.api_domain, obj, headers);
  }
}
