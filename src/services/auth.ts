import {EventEmitter, Injectable, ViewChild} from "@angular/core";
import {AlertController, NavController} from "ionic-angular";
import {Stop} from "../models/stop";
import {ApiService} from "./api";
import {UserInfo} from "../models/userInfo";
import {StorageService} from "./storage";
import {Trip} from "../models/trip";

@Injectable()
export class AuthService {
  public userInfo: UserInfo;
  private user_email: string = ''; // TODO: this will go into storage in future
  @ViewChild('myNav') nav: NavController;
  public midnightInterval: any; // used in setInterval

  unsubscribeAllEmitter: EventEmitter<boolean> = new EventEmitter();
  authUpdated: EventEmitter<boolean> = new EventEmitter();
  endDay: EventEmitter<boolean> = new EventEmitter();
  refreshDataEmitter: EventEmitter<boolean> = new EventEmitter<boolean>();

  constructor(private storageService: StorageService,
              private alertCtrl: AlertController,
              private api: ApiService) {
    this.checkUserInfo();
  }


  /**
   * Check if there is user information already in storage.
   * If not, validate the token we have on the server, and if valid, get the user information from the server.
   */
  checkUserInfo() {
    this.api.validateToken()
      .then(data => {
        console.log('after validate token response', data);
        if (data['message'] == 'valid token') {
          this.storageService.getUserInfo(null)
            .then((userInfo: UserInfo) => {
              if (userInfo != null) {
                this.userInfo = userInfo;
                this.updateAuth(true);
              } else {
                // get from server
                this.api.fetch('getUserInfo', {user_id: this.userInfo.user_id, access_token: this.userInfo.access_token}, true)
                  .subscribe(
                    (userInfo: UserInfo) => {
                      this.userInfo = userInfo;
                      this.storageService.set('userInfo', this.userInfo)
                        .then(() => {
                          this.updateAuth(true);
                        })
                        .catch(err => {
                          console.log('auth: unable to set userInfo in storage', err);
                          this.updateAuth(false);
                        });
                    },
                    err => {
                      console.log('auth: unable to fetch user data', err);
                      this.updateAuth(false);
                    });
              }
            })
            .catch(err => {
              console.log('auth: unable to get userInfo from storage', err);
              this.updateAuth(false);
            });
        } else {
          //console.log('auth: token was invalid');
          this.updateAuth(false);
        }
      })
      .catch(err => {
        console.log('auth: could not validate token', err);
        this.updateAuth(false);
      });
  }

  /**
   * Used when app comes back from sleep state
   * to check for new data from Fusion
   */
  resyncData() {
    this.api.validateToken()
      .then(data => {
        console.log('auth, resync: after validate token response', data);
        if (data['message'] == 'valid token') {
          // tell the pages
          this.refreshDataEmitter.emit(true);

          /*if (this.userInfo.user_id != null) {
            this.api.fetch('getUserInfo', {user_id: this.userInfo.user_id, access_token: this.userInfo.access_token}, true)
              .subscribe(
                (userInfo: UserInfo) => {
                  this.userInfo = userInfo;
                  this.storageService.set('userInfo', this.userInfo)
                    .then(() => {
                      console.log('auth, resync: got new userInfo and set in storage', userInfo);

                      // tell the pages
                      this.refreshDataEmitter.emit(true);
                    })
                    .catch(err => {
                      console.log('auth, resync: unable to set userInfo in storage', err);
                    });
                },
                err => {
                  console.log('auth, resync: unable to fetch user data', err);
                }
              );
          } else {
            console.log('auth: no user_id in userInfo');
            this.updateAuth(false);
          }*/
        } else {
          console.log('auth, resync: do not have a valid token');
          this.updateAuth(false);
        }
      })
      .catch(err => {
        console.log('auth: could not validate token', err);
        this.updateAuth(false);
      });
  }

  /**
   * Update auth as true or false (logged in or not).
   * Emit this value to root (app.component.ts) to set the root to login or tabs.
   *
   * @param {boolean} isAuthenticated
   */
  updateAuth(isAuthenticated: boolean) {
    if (isAuthenticated == null) {
      this.api.validateToken()
        .then(data => {
          if (data['message'] == 'valid token') {
            if (this.userInfo.user_id != null) {
              this.api.fetch('getUserInfo', {user_id: this.userInfo.user_id, access_token: this.userInfo.access_token}, true)
                .subscribe(
                  (userInfo: UserInfo) => {
                    this.userInfo = userInfo;
                    this.storageService.set('userInfo', this.userInfo)
                      .then(() => {
                        this.updateAuth(true);
                      })
                      .catch(err => {
                        console.log('auth: unable to set userInfo in storage', err);
                        this.updateAuth(false);
                      });
                  },
                  err => {
                    console.log('auth: unable to fetch user data', err);
                    this.updateAuth(false);
                  }
                );
            } else {
              console.log('auth: no userInfo available');
              this.updateAuth(false);
            }
          } else {
            //console.log('auth: not a valid token');
            this.updateAuth(false);
          }
        })
        .catch(err => {
          console.log('auth: unable to validate token', err);
          this.updateAuth(false);
        });
    } else {
      if (!isAuthenticated) {
        this.storageService.set('userInfo', null)
          .then(() => {
            this.authUpdated.emit(isAuthenticated);
          });
      } else {
        this.authUpdated.emit(isAuthenticated);
      }
    }
  }

  /**
   * Login, wet userInfo
   *
   * @param {string} username
   * @param {string} password
   */
  login(username: string, password: string) {
    return new Promise<any>((resolve, reject) => {
      this.api.fetch('login', {username: username, password: password}, false)
        .subscribe(
          (userInfo: UserInfo) => {
            console.log('auth: login success', userInfo);
            this.userInfo = userInfo;
            this.storageService.set('userInfo', userInfo)
              .then(() => {
                this.api.setAccessToken(userInfo['access_token'])
                  .then(() => {
                    this.setEndOfDayTimeout();
                    resolve(userInfo);
                  })
                  .catch(err => {
                    console.log('api: login could not set access token', err);
                    reject(err);
                  });
              })
              .catch(err => {
                console.log('api: login could not set userInfo in storage', err);
                reject(err);
              });
          },
          err => {
            console.log('api: could not log in', err.error.message);
            reject(err);
          });
    });
  }

  /**
   * Log out user.
   * Utilizes api fetch.
   */
  logout() {
    console.log('calling new promise');
    let url = 'logout';

    return new Promise<any>((resolve, reject) => {

      this.api.fetch(url, {username: this.userInfo.username, user_id: this.userInfo.user_id}, true)
        .subscribe(
          data => {
            this.clearUserInfo()
              .then(() => {
                /**
                 * Emitter to tell all of the miroute driving, stopped, etc.. pages to unsubscribe
                 */
                this.unsubscribeAllEmitter.emit(true);
                resolve(data)
              })
              .catch(err => {
                reject(err);
              });
          },
          err => reject(err)
        );
    });
  }

  /**
   * Return data held locally (not in storage).
   *
   * @returns {object}
   */
  getUserInfo() {
    let me = this;
    return new Promise<any>((resolve, reject) => {
      if (me.userInfo != null) {
        resolve(<UserInfo>me.userInfo);
      } else {
        // check storage
        me.storageService.get('userInfo')
          .then((userInfo: UserInfo) => resolve(<UserInfo>userInfo))
          .catch(err => {
            console.log('auth: could not get userInfo in storage', err);

            // get it from the server
            me.fetchUserInfo()
              .then((userInfo: UserInfo) => {
                // fetch already set this in storage and locally
                resolve(userInfo);
              })
              .catch(err => {
                // if we couldn't get user info we should log out now!
                console.log('auth: could not fetch userInfo', err);
                const alert = me.alertCtrl.create({
                  title: 'No User Information Available',
                  message: "We don't seem to have any user information for you available. Please try again shortly.",
                  buttons: ['OK']
                });
                alert.present()
                  .then(() => {
                  });

                alert.onDidDismiss(() => {
                  me.nav.popToRoot()
                    .then(() => {
                      reject(err);
                    });
                });
              });
          });
      }
    });
  }

  /**
   * Return to server for updated userInfo
   */
  fetchUserInfo() {
    console.log('auth: before fetchUserInfo', this.userInfo);
    return new Promise<any>((resolve, reject) => {
      if (this.userInfo.user_id != null) {
        this.api.fetch('getUserInfo', {user_id: this.userInfo.user_id, access_token: this.userInfo.access_token}, true)
          .subscribe(
            (userInfo: UserInfo) => {
              this.storageService.set('userInfo', userInfo)
                .then(() => {
                  resolve(userInfo);
                });
            },
            err => {
              console.log('auth: getchUserInfo fail', err);
              reject(err);
            }
          );
      } else {
        console.log('auth: fetchUserInfo No user_id in userInfo');
        this.updateAuth(false);
      }
    });
  }

  /**
   * Used when user forgot password to get reset code.
   */
  validateEmail(email: string) {
    return new Promise<any>((resolve, reject) => {
      this.api.fetch('validateEmailForResetCode', {'email': email}, false)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  /**
   * User when user forgot password.
   * Temporary code sent to personal email to validate user against email.
   *
   * @param {number} code
   * @returns {Observable<Object>}
   */
  validateCode(code: number) {
    let me = this;
    return new Promise<any>((resolve, reject) => {
      me.api.fetch('validateResetCode', {'email': this.user_email, 'code': code}, false)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  /**
   * Used during password reset process, final step to send new password to server.
   *
   * @param {string} password
   * @returns {Observable<Object>}
   */
  submitNewPassword(password: string) {
    let me = this;

    return new Promise<any>((resolve, reject) => {
      me.api.fetch('submitNewPassword', {'email': this.user_email, 'password': password}, false)
        .subscribe(
          data => {
            resolve(data);
          },
          err => {
            reject(err);
          }
        );
    });
  }

  /**
   * Post-login, set a timeout to force an end-of-day at midnight.
   */
  setEndOfDayTimeout() {
    // reset any existing interval
    if (this.midnightInterval) {
      clearTimeout(this.midnightInterval);
    }

    let now = new Date();
    let midnight = new Date(now.getFullYear(), now.getMonth(), now.getDate(), 23, 59, 59, 0);

    let timeTillMidnight = <number>midnight.getTime() - <number>now.getTime();

    this.midnightInterval = setTimeout(() => {
      const alert = this.alertCtrl.create({
        title: 'End Your Day',
        message: 'Your day has ended. To calculate information for your trips for today, please enter in an ending odometer',
        buttons: ['Enter Ending Odometer']
      });
      alert.present()
        .then(()=>{});

      alert.onDidDismiss(() => {
        this.endDay.emit(true);
      });
    }, timeTillMidnight); // timeTillMidnight
  }

  /**
   * End driver's previously un-ended day with last odometer
   *
   * @param {string} day
   * @param {number} odometer
   * @param page
   */
  submitEndingOdometer(day: string, odometer: number, page: string) {
    let me = this;

    let url = page == 'end_previous_day' ? 'endPreviousDay' : 'endDay';

    return new Promise<any>((resolve, reject) => {
      this.storageService.getUserInfo('user_id')
        .then((user_id: string) => {
          me.api.fetch(url, {user_id: user_id, ending_day: day, ending_odometer: odometer}, true)
            .subscribe(
              data => resolve(data),
              err => reject(err)
            );
        });
    });
  }

  /**
   * Get all stops within certain distance (specified server-side) of current lat&lng
   *
   * @param {number} latitude
   * @param {number} longitude
   */
  getNearbyStops(latitude: number, longitude: number) {
    return new Promise<any>((resolve, reject) => {
      this.api.fetch('getNearbyStops', {
        user_id: this.userInfo.user_id,
        company_id: this.userInfo.company_id,
        latitude: latitude,
        longitude: longitude
      }, true)
        .subscribe(
          (stops: Stop[]) => resolve(stops),
          err => reject(err)
        );
    });
  }

  /**
   * Submit new stop to server for driver from saved stop modal.
   *
   * @param {Stop} stop
   * @param user_id
   */
  submitNewStop(stop: Stop, user_id: number) {
    let me = this;

    const body = {stop: stop, user_id: user_id};
    console.log("auth: before submit new stop", body);

    return new Promise<any>((resolve, reject) => {
      me.api.fetch('submitNewStop', body, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }

  clearUserInfo() {
    return new Promise<any>((resolve, reject) => {
      this.storageService.set('userInfo', <UserInfo>{})
        .then(() => resolve('done'))
        .catch(err => {
          console.log('auth: clearUserInfo error', err);
          reject(err);
        });
    });
  }

  resumeDriving(trip_date) {
    let me = this;
    let url = 'resumeDriving';

    return new Promise<any>((resolve, reject) => {
      me.api.fetch(url, {trip_date: trip_date}, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }

  updateDailyTrip(stop_log_id: number, business_purpose: string, mileage_adjusted: number, mileage_comment: string, trip_date: string) {
    console.log('auth: updateDailyTrip got stop_log_id', stop_log_id);
    return new Promise<any>((resolve, reject) => {
      this.api.fetch('updateDailyTrip', {
        user_id: this.userInfo.user_id,
        stop_log_id: stop_log_id,
        business_purpose: business_purpose,
        mileage_adjusted: mileage_adjusted,
        mileage_comment: mileage_comment,
        trip_date: trip_date
      }, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }

  getLastEndingOdometer() {
    let me = this;
    let url = 'getLastEndingOdometer';

    return new Promise<any>((resolve, reject) => {
      this.storageService.getUserInfo('user_id')
        .then((user_id: number) => {
          me.api.fetch(url, {user_id: user_id}, true)
            .subscribe(
              data => resolve(data),
              err => reject(err)
            );
        });
    });
  }

  updateTrip(trip: Trip) {
    let url = 'updateDailyTrip';
    return new Promise<any>((resolve, reject) => {
      this.api.fetch(url, {trip: trip}, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }

  deleteTrip(trip_id: number) {
    console.log('auth: calling deleteDailyTrip with trip_id ' + trip_id);
    let url = 'deleteDailyTrip';
    return new Promise<any>((resolve, reject) => {
      this.api.fetch(url, {trip_id: trip_id}, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }

  updateLastState(dataObj: Object) {
    let url = 'updateDailyMileageLastState';
    return new Promise<any>((resolve, reject) => {
      this.api.fetch(url, dataObj, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }

  apiLog(message: string) {
    return new Promise<any>((resolve, reject) => {
      this.api.fetch('apiLog', {user_id: this.userInfo.user_id, message: message}, true)
        .subscribe(
          data => resolve(data),
          err => reject(err)
        );
    });
  }
}
