import {Storage} from "@ionic/storage";
import {UserInfo} from "../models/userInfo";
import {Injectable} from "@angular/core";
import {Trip} from "../models/trip";

@Injectable()
export class StorageService {

  constructor(private storage: Storage) {

  }

  updateUserInfo(data: object) {
    let me = this;
    return new Promise<any>((resolve, reject) => {
      me.storage.get('userInfo')
        .then((userInfo: UserInfo) => {
          console.log('userInfo before: ', userInfo);
          userInfo = Object.assign(userInfo, data);
          console.log('userInfo after', userInfo);
          me.storage.set('userInfo', userInfo)
            .then(() => resolve(userInfo))
        })
        .catch(err => reject(err));
    });
  }

  getUserInfo(field: any) {
    //console.log('storage, field: ',field);
    return new Promise<any>((resolve, reject)=>{
      this.storage.get('userInfo')
        .then((userInfo: UserInfo)=>{
          console.log('storage: looking for ' + field + ' in ', userInfo);
          if (userInfo == null || userInfo['message'] == 'not implemented') {
            console.log('storage: rejecting no userInfo');
            reject('there is no user info');
          } else if (!field) {
            console.log('storage: resolving userInfo');
            resolve(userInfo);
          } else if (userInfo[field]) {
            console.log('storage: resolving ' + field + ' in userInfo');
            resolve(userInfo[field]);
          } else if (field == 'last_stop_name') {
            if (userInfo.trips.length) {
              let lastTrip: Trip = userInfo.trips.slice(-1)[0];
              this.updateUserInfo({last_stop_name: lastTrip.stop.name})
                .then(() => {
                  resolve(lastTrip.stop.name);
                });
            } else {
              reject('could not find last_stop_name');
            }
          } else {
            console.log('storage: rejecting field not in userInfo');
            reject();
          }
        });
    });
  }

  get(field: string) {
    return this.storage.get(field);
  }

  set(field: string, value: any) {
    //console.log('setting field ' + field + ' to ', value);
    return this.storage.set(field, value);
  }
}
