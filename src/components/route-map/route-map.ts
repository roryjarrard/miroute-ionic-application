import {Component, ElementRef, Input, OnInit, SimpleChanges, ViewChild} from '@angular/core';
import {Trip} from "../../models/trip";

declare var google;

@Component({
  selector: 'route-map',
  templateUrl: 'route-map.html'
})
export class RouteMapComponent implements OnInit {
  @Input('trips') trips : Trip[];
  @ViewChild('routemap') mapElement: ElementRef;
  map: any;
  directionsService = new google.maps.DirectionsService;
  directionsDisplay = new google.maps.DirectionsRenderer;
  markers = [];
  clickableInfoWindows = [];
  clickableMarkers = [];

  constructor() {

  }

  ngOnInit() {
      console.log('route-map: onInit with trip', this.trips);
      this.loadMap();
  }

  ngOnChanges(changes: SimpleChanges) {
    console.log('changes', changes);
    this.loadMap();
  }

  /**
   * Display map in this order:
   * - multiple trips: route of trips
   * - 1 trip: map with trip as center and marker
   * - 0 trips: map with current location
   */
  loadMap() {
    if (!this.trips || this.trips.length === 0) {
      return;
    }
    console.log('review: loading map');
    this.markers = [];
    let center = {lat: 0, lng: 0};

    if (this.trips.length > 1) {
      // console.log('stops > 1')
      this.trips.forEach((trip: Trip) => {
        let lat = trip.stop.latitude;
        let lng = trip.stop.longitude;
        this.markers.push({lat: lat, lng: lng});
      });

      center = {lat: this.markers[0].lat, lng: this.markers[0].lng};
    } else {
      // trips == 1
      const trip = <Trip>this.trips[0];
      let lat = trip.stop.latitude ? trip.stop.latitude : 0;
      let lng = trip.stop.longitude ? trip.stop.longitude : 0;
      center = {lat: lat, lng: lng};

      this.markers.push(center);
    }

    let mapOptions = {
      center: center,
      zoom: 7,
      mapTypeId: google.maps.MapTypeId.ROADMAP,
      gestureHandling: 'cooperative',
      zoomControl: true,
      streetViewControl: false,
      fullscreenControl: false,
      mapTypeControl: false
    };

    this.map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
    this.directionsDisplay.setMap(this.map);

    if (this.markers.length) {
      this.drawRoute();
    }
  }

  /**
   * Draws the route when trips exist
   */
  drawRoute() {
    let last_index = this.markers.length - 1;
    let start = new google.maps.LatLng(this.markers[0].lat, this.markers[0].lng);
    let end = new google.maps.LatLng(this.markers[last_index].lat, this.markers[last_index].lng);

    let waypoints = [];

    // case 1 marker
    if (last_index === 0) {
      let marker = new google.maps.Marker({
        position: start,
        map: this.map,
        title: 'I am a title'
      });
      marker.setMap(this.map);
    }
    else {
      // case 3 or more markers, waypoints (2 markers have no waypoints)
      if (last_index > 1) {
        for (let i = 1; i < last_index; i++) {
          waypoints.push({location: new google.maps.LatLng(this.markers[i].lat, this.markers[i].lng)});
        }
      }

      let request = {
        origin: start,
        destination: end,
        waypoints: waypoints,
        /*provideRouteAlternatives: this.editing_route,*/
        optimizeWaypoints: false,
        travelMode: google.maps.TravelMode.DRIVING
      };

      let self = this;

      // console.log('request: ',request)

      this.directionsService.route(request, function (response, status) {
        // console.log('response ok: ',status)
        // console.log('response: ',response)

        if (status == google.maps.DirectionsStatus.OK) {

          // console.log('response was OK')

          if (response.routes.length < 1) return; // no routes

          if (response.routes.length > 1) { // more than 1 route
            new google.maps.DirectionsRenderer({ // map will NOT center w/out this
              suppressMarkers: true, // don't show default markers
              map: self.map,
              directions: response,
              polylineOptions: {visible: false}, // hide polyline
            });
            self.renderDirectionsPolylines(response); // polylines set on map here

            // create start & end markers
            let leg = response.routes[0].legs[0];
            self.generateStartMarker(leg);
            self.generateEndMarker(leg);

          } else { // if 1 route only
            new google.maps.DirectionsRenderer({ // create directions
              suppressMarkers: true, // don't show default markers
              map: self.map,
              directions: response,
              polylineOptions: {strokeColor: '#052460', strokeWeight: 5, strokeOpacity: 0.8},
            });

            let legs = response.routes[0].legs;
            self.createMarkersWithInfoWindowsUsingLegs(legs);
          }
        } else {
          // other code
        }
      });
    }
  }

  /**
   * Taken directly from Fusion code for GoogleMap component.
   *
   * @param response
   * @param {any} route_index
   */
  renderDirectionsPolylines(response, route_index = null) {
    let routes = response.routes;
    let polylines = [];

    console.log('routes: ', routes);
    // console.log('editing_route_mileage: ',this.editing_route_mileage);

    //let startIndex = 0;
    for (let i = 0; i < routes.length; i++) {
      /*let route_distance = routes[i].legs[0].distance.text.replace(' mi', '');*/
      // let adjusted_route_distance = route_distance_text.replace(' mi', '');

      // console.log(route_distance);

      // let matching_route_index = i;
      /*if (route_index === null && this.editing_route_mileage === route_distance) { // if route_being_edited mileage == route[i]
          // console.log('editing_route_mileage: ',this.editing_route_mileage,'route_distance: ',route_distance,'route_index before: ',route_index)
          route_index = i;
          // console.log('route_index after: ',route_index)
          console.log('matches', route_index)
      } else {
          route_index = null;
          console.log('NOT matching',route_index)
      }*/

      // Multi-ternary
      let strokeColor = i === 0 && !route_index ? 'red' : i === route_index ? 'red' : '#333';
      let strokeOpacity = i === 0 && !route_index ? 1 : i === route_index ? 1 : 0.3;
      let zIndex = i === 0 && !route_index ? 5 : i === route_index ? 5 : 1;

      // console.log(route_index,strokeColor,strokeOpacity,zIndex)

      let overview_path = routes[i].overview_path; // an array of { lat: #, lng: # } pairs

      /*let infoWindow = new google.maps.InfoWindow;
      infoWindow.setContent();*/

      let infoWindow = new google.maps.InfoWindow();

      // terminal east - door 503
      let flightPath = new google.maps.Polyline({
        path: overview_path,
        strokeColor: strokeColor,
        strokeOpacity: strokeOpacity,
        strokeWeight: 8,
        zIndex: zIndex,
        infoWindow: infoWindow,
        infoWindowPos: 50,
        map: this.map
      });

      polylines.push(flightPath);

      let infoWindowPosition = i === 0 ? overview_path[Math.round(overview_path.length / 5)]
        : i === 1 ? overview_path[Math.round(overview_path.length / 2)]
          : overview_path[overview_path.length - Math.round(overview_path.length / 5)];

      let infoWindowActive = i === 0 && !route_index ? 'active' : i === route_index ? 'active' : '';

      infoWindow.setContent('<div class="infowWindowContent ' + infoWindowActive + '">' + routes[i].legs[0].distance.text + '</div>');
      infoWindow.setPosition(infoWindowPosition);
      // infoWindow.setOptions({pixelOffset: new google.maps.Size(0,-30)});
      infoWindow.open(this.map);

      /*google.maps.event.addListener(flightPath,'click', function(evt) {
        self.new_route_mileage = route_distance;


        polylines.forEach(polyline => { // remove all polylines from map
          polyline.setMap(null);
        });

        self.renderDirectionsPolylines(response, i); // rerender polylines
      });*/

      // infoWindows click Event
      /*google.maps.event.addListener(infoWindow, 'closeclick', function() {
        self.new_route_mileage = route_distance;
        Common.fire(self.caller_id + '-new-google-map-route-selected', self.new_route_mileage);

        polylines.forEach(polyline => { // remove all polylines from map
          polyline.setMap(null);
        });

        self.renderDirectionsPolylines(response, i);
      });*/
    } // end for
  }

  generateStartMarker(leg) {
    let self = this;
    let start_address_pieces = leg.start_address.split(",");
    let start_address = start_address_pieces[0]
      + '<br>'
      + start_address_pieces[1]
      + ', '
      + start_address_pieces[2];

    self.makeClickableMarker(leg.start_location,
      self.createRegularFlagMarker('green'),
      '<div class="infowWindowContent">' + start_address + '</div>'
    );
  }

  generateEndMarker(leg) {
    let self = this;
    let end_address_pieces = leg.end_address.split(",");
    let end_address = end_address_pieces[0]
      + '<br>'
      + end_address_pieces[1]
      + ', '
      + end_address_pieces[2];
    self.makeClickableMarker(leg.end_location,
      self.createCheckeredFlagMarker('#000'),
      '<div class="infowWindowContent">' + end_address + '</div>'
    );
  }

  createMarkersWithInfoWindowsUsingLegs(legs) {
    let self = this;
    for (let l = 0; l < legs.length; l++) {
      if (l == 0) { // Start Location
        let leg = legs[l];
        self.generateStartMarker(leg);
      }
      if (l == legs.length - 1) { // End Location
        let leg = legs[l];
        self.generateEndMarker(leg);
      } else { // Stop Locations
        let stop_leg = legs[l];
        let stop_address_pieces = stop_leg.end_address.split(",");
        let stop_address = stop_address_pieces[0]
          + '<br>'
          + stop_address_pieces[1]
          + ', '
          + stop_address_pieces[2];

        // need to fix data in database - if there's an end stop a marker is shown as well as the checkered flag
        // last conversation with Sterling & Rory (6-2-18)
        // console.log([l]+' - leg lat: ',legs[l].end_location.lat(),'leg lng: ',legs[l].end_location.lng());
        // console.log([l]+' - stop lat: ',this.stops[this.stops.length - 1].latitude,'stop lng: ',this.stops[this.stops.length - 1].longitude);

        self.makeClickableMarker(stop_leg.end_location,
          self.createGoogleMarker('#000'),
          '<div class="infowWindowContent">' + stop_address + '</div>',
          true, (l + 1)
        );
      }
    }
  }

  makeClickableMarker(position, icon, content, stopBetween = false, index = null) {
    if (stopBetween) { // if not start or end
      let marker = new google.maps.Marker({
        position: position,
        map: this.map,
        label: {color: '#000', fontSize: '12px', fontWeight: '600', text: index.toString()},
      });
      this.createMarkerArrayInfoWindowArrayAndAddClickListeners(this, marker, content);
    } else { // start & end
      let marker = new google.maps.Marker({
        position: position,
        map: this.map,
        icon: icon,
      });
      this.createMarkerArrayInfoWindowArrayAndAddClickListeners(this, marker, content);
    }
  }

  createRegularFlagMarker(color) {
    return { // fa-flag from Font Awesome 5 Github
      path: 'M349.565 98.783C295.978 98.783 251.721 64 184.348 64c-24.955 0-47.309 4.384-68.045 12.013a55.947 55.947 0 0 0 3.586-23.562C118.117 24.015 94.806 1.206 66.338.048 34.345-1.254 8 24.296 8 56c0 19.026 9.497 35.825 24 45.945V488c0 13.255 10.745 24 24 24h16c13.255 0 24-10.745 24-24v-94.4c28.311-12.064 63.582-22.122 114.435-22.122 53.588 0 97.844 34.783 165.217 34.783 48.169 0 86.667-16.294 122.505-40.858C506.84 359.452 512 349.571 512 339.045v-243.1c0-23.393-24.269-38.87-45.485-29.016-34.338 15.948-76.454 31.854-116.95 31.854z',
      anchor: new google.maps.Point(60, 500),
      fillColor: color,
      fillOpacity: 1,
      strokeColor: '#000',
      strokeWeight: 1,
      // scale: .07,
      scale: .05,
    };
  }

  createCheckeredFlagMarker(color) {
    return { // fa-flag-checkered from Font Awesome 5 Github
      path: 'M466.515 66.928C487.731 57.074 512 72.551 512 95.944v243.1c0 10.526-5.161 20.407-13.843 26.358-35.837 24.564-74.335 40.858-122.505 40.858-67.373 0-111.63-34.783-165.217-34.783-50.853 0-86.124 10.058-114.435 22.122V488c0 13.255-10.745 24-24 24H56c-13.255 0-24-10.745-24-24V101.945C17.497 91.825 8 75.026 8 56 8 24.296 34.345-1.254 66.338.048c28.468 1.158 51.779 23.968 53.551 52.404.52 8.342-.81 16.31-3.586 23.562C137.039 68.384 159.393 64 184.348 64c67.373 0 111.63 34.783 165.217 34.783 40.496 0 82.612-15.906 116.95-31.855zM96 134.63v70.49c29-10.67 51.18-17.83 73.6-20.91v-71.57c-23.5 2.17-40.44 9.79-73.6 21.99zm220.8 9.19c-26.417-4.672-49.886-13.979-73.6-21.34v67.42c24.175 6.706 47.566 16.444 73.6 22.31v-68.39zm-147.2 40.39v70.04c32.796-2.978 53.91-.635 73.6 3.8V189.9c-25.247-7.035-46.581-9.423-73.6-5.69zm73.6 142.23c26.338 4.652 49.732 13.927 73.6 21.34v-67.41c-24.277-6.746-47.54-16.45-73.6-22.32v68.39zM96 342.1c23.62-8.39 47.79-13.84 73.6-16.56v-71.29c-26.11 2.35-47.36 8.04-73.6 17.36v70.49zm368-221.6c-21.3 8.85-46.59 17.64-73.6 22.47v71.91c27.31-4.36 50.03-14.1 73.6-23.89V120.5zm0 209.96v-70.49c-22.19 14.2-48.78 22.61-73.6 26.02v71.58c25.07-2.38 48.49-11.04 73.6-27.11zM316.8 212.21v68.16c25.664 7.134 46.616 9.342 73.6 5.62v-71.11c-25.999 4.187-49.943 2.676-73.6-2.67z',
      anchor: new google.maps.Point(60, 500),
      fillColor: color,
      fillOpacity: 1,
      strokeColor: '#000',
      strokeWeight: 0,
      // scale: .07,
      scale: .05,
    };
  }

  createGoogleMarker(color) {
    return {
      fillOpacity: 1,
      fillColor: '#fff',
      strokeOpacity: 1,
      strokeWeight: 1,
      strokeColor: '#000',
      // scale: 15,
      scale: 6,
    };
  }

  createMarkerArrayInfoWindowArrayAndAddClickListeners(context, marker, content) {
    this.clickableMarkers.push(marker); // create marker array

    let infoWindow = new google.maps.InfoWindow({ // create infoWindow
      content: content
    });
    this.clickableInfoWindows.push(infoWindow); // create infoWindow array

    let self = context; // Vue instance
    marker.addListener('click', function () {
      let me = this; // what was clicked
      self.clickableMarkers.forEach((clickableMarker, index) => {
        if (me.position != clickableMarker.position) { // if doesn't match clickableMarker clicked
          self.clickableInfoWindows[index].close(self.map, clickableMarker); // close infoWindow
        }
      });
      infoWindow.open(self.map, marker); // open infoWindow
    });
  }
}
