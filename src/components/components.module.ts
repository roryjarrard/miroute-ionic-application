import { NgModule } from '@angular/core';
import { RouteMapComponent } from './route-map/route-map';
@NgModule({
	declarations: [RouteMapComponent],
	imports: [],
	exports: [RouteMapComponent]
})
export class ComponentsModule {}
