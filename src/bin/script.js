var fs = require('fs');

function readWriteSync() {
  let env = process.env.ENV;
  if (!process.env.ENV) {
    env = 'stage5';
  }

  /*if (env == 'prod') {
    document.write('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBXceEn7NTkUgQ9viSyBsWBNL5CuC3W-gc"></script>');
  } else {
    document.write('<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyB4cVyUmAOKJC0foRNHAjROST06W7U8Iow"></script>');
  }*/

  var data = fs.readFileSync(`src/environments/environment.${env}.ts`, 'utf-8');
  fs.writeFileSync('src/environments/environment.ts', data, 'utf-8');
  console.log('readFileSync complete');
}

readWriteSync();
