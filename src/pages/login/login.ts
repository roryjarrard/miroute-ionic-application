import {Component} from '@angular/core';
import {AlertController, LoadingController, NavController, Platform} from "ionic-angular";
import {RegistrationPage} from "../registration/registration";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../services/auth";
import {ForgotVerifyPage} from "../forgot/forgot-verify/forgot-verify";
import {StorageService} from "../../services/storage";
import {CallNumber} from "@ionic-native/call-number";
// import {state, trigger, style} from "@angular/animations";
import {Observable} from "rxjs";

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
  // animations: [
  //   trigger('keyboardShowing', [
  //     state('hidden',
  //       style({
  //
  //       })
  //     ),
  //     state('shown',
  //       style({
  //         // bottom: '120px'
  //       })
  //     ),
  //   ])
  // ]
})
export class LoginPage {
  username: string;
  password: string;
  remember_me: boolean = false;
  hide_content: boolean = false;
  visibility: string = 'shown';
  keyboard_height = 0;

  constructor(private navCtrl: NavController,
              private authService: AuthService,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private storageService: StorageService,
              private callNumber: CallNumber,
              private _platform: Platform) {

    this._platform.ready().then(() => {

      Observable.fromEvent(window, 'keyboardWillShow').subscribe( (event: any) => {
        if (this._platform.is('ios')) {
          document.getElementById('loginFormArea').style.bottom = '' + (event.keyboardHeight + 5) + 'px';
        } else {
          // document.getElementById('loginFormArea').style.bottom = '5px';
        }
      });

      Observable.fromEvent(window, 'keyboardWillHide').subscribe((event: any) => {
        // document.getElementById('loginFormArea').setAttribute('bottom', )
        document.getElementById('loginFormArea').style.bottom = '120px';
        this.visibility = 'shown';
      });
    });

    this.storageService.get('remember_me')
      .then((remember_me: boolean) => {

        this.remember_me = remember_me;

        if (remember_me) {
          this.storageService.get('login_username').then((username: string) => {
            this.username = username;
          });

          this.storageService.get('login_password').then((password: string) => {
            this.password = password;
          });
        }
      });
  }


  prefillUsernameAndPassword() {

  }

  onToggleRemember(event) {
    this.remember_me = event['value'];
    this.storageService.set('remember_me', event['value']);
  }

  onForgot() {
    this.navCtrl.push(ForgotVerifyPage);
  }

  onRegistration() {
    this.navCtrl.push(RegistrationPage);
  }

  onLogin(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Logging in...'
    });
    loading.present();

    this.authService.login(form.value.username, form.value.password)
      .then(data => {
        loading.dismiss();

        console.log('login: login data', data);

        if (this.remember_me) {
          this.storageService.set('login_username', form.value.username);
          this.storageService.set('login_password', form.value.password);
        } else {
          this.storageService.set('login_username', null);
          this.storageService.set('login_password', null);
        }

        this.authService.updateAuth(true);
      })
      .catch(err => {
        loading.dismiss();

        console.log('login: unable to login', err, err.error.message);

        let message = err['status'] == 0 ? 'Unable to contact login server' : 'Unable to validate username and password';

        if (err['error']['message'].includes('not a valid user') || err['error']['message'].includes('login terminated')) {
          message = 'Your account has been inactivated. If you need assistance please contact your CSA or contact CarData Support.'
        }
        if (err['error']['message'].includes('login yellow')) {
          message = "Please login to CarData online at <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> and set up your driver profile.";
        }

        if (err['error']['message'].includes('not mi-route')) {
          message = "You are not allowed to use Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.";
        }

        if (err['error']['message'].includes('excluded mi-route')) {
          message = "You are excluded from Mi-Route. Please use <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a> instead.";
        }

        const alert = this.alertCtrl.create({
          title: 'Login Failed',
          message: message,
          buttons: ['OK']
        });
        alert.present();

        alert.onDidDismiss(() => {
          this.authService.updateAuth(false);
        });
      });
  }

  mailTo() {
    console.log('about to launch email');
    this._platform.ready().then(() => {
      console.log('launching email');
      if (this._platform.is('ios')) {
        console.log('PLATFORM IS IOS');
        window.open('mailto:support@cardataconsultants.com', '_self');
      } else {
        window.open('mailto:support@cardataconsultants.com');
      }
    });
  }

  launchDialer() {
    console.log('launchDialer');
    this._platform.ready().then(() => {

      if (this._platform.is('mobileweb')) {
        document.location.href = "tel:18665505188";
      } else {
        this.callNumber.callNumber('1 866 550 5188', true)
          .then(() => {
            console.log('Lanched dialer');
          })
          .catch(err => {
            console.log('Error launching dialer', err);
          });
      }
    });
  }
}
