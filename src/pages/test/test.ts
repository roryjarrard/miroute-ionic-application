import {Component} from '@angular/core';
import {UserInfo} from "../../models/userInfo";
import {StorageService} from "../../services/storage";

@Component({
  selector: 'page-test',
  templateUrl: 'test.html',
})
export class TestPage {
  constructor(private storageService: StorageService) {
    this.storageService.get('userInfo')
      .then((userInfo: UserInfo)=>{
        console.log('TEST: userInfo', userInfo);
      });

    this.storageService.get('last_tab')
      .then(tab => {
        console.log('TEST: last tab: ' + tab);
      });

    this.storageService.get('last_page')
      .then(page => {
        console.log('TEST: last page: ' + page);
      });
  }
}
