import {Component, Input} from '@angular/core';

/**
 * Generated class for the MirouteHeaderPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-miroute-header',
  templateUrl: 'miroute-header.html',
})
export class MirouteHeaderPage {
  header_data: any;

  constructor() {
  }

  @Input()
  set header(header_data: any) {
    this.header_data=header_data;
  }

  get header() {
    return this.header_data;
  }
}
