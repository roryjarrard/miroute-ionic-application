import { Component } from '@angular/core';

/**
 * Generated class for the MileageEntryPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-mileage-entry',
  templateUrl: 'mileage-entry.html',
})
export class MileageEntryPage {

  constructor() {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MileageEntryPage');
  }

}
