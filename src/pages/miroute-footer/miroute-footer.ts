import {Component, Input} from '@angular/core';

/**
 * Generated class for the MirouteFooterPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@Component({
  selector: 'page-miroute-footer',
  templateUrl: 'miroute-footer.html',
})
export class MirouteFooterPage {
  footer_data: any;

  constructor() {
  }

  @Input()
  set footer(footer_data: any) {
    this.footer_data=footer_data;
  }

  get footer() {
    return this.footer_data;
  }
}
