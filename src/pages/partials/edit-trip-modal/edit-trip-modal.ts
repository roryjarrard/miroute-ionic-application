import {Component, OnInit} from '@angular/core';
import {AlertController, NavParams, Platform, ViewController} from "ionic-angular";
import {Trip} from "../../../models/trip";
import {AuthService} from "../../../services/auth";

@Component({
  selector: 'page-edit-trip-modal',
  templateUrl: 'edit-trip-modal.html',
})
export class EditTripModalPage implements OnInit {
  //region DATA MEMBERS
  // all members passed in to modal controller
  trip: Trip;                           // specific trip to be edited
  mileage_label: string;                // km or mi based on company
  original_mileage_adjustment: number;  // keep original in case we cancel
  combined_mileage: number;             // hold sum of original and adjusted mileage
  pre_mileage_adjusted;                 // must be a number, if original was null, then 0
  platform;                             // ios handles input differently (used in html)
  use_stop_contract: number = 0;        // for contract companies
  use_tickets: number = 0;              // for contract companies, specific drivers only
  submit_disabled: boolean;             // disable the submit button to not allow double-submit
  //endregion

  /**
   * Constructor sets a platform instance
   *
   * @param navParams
   * @param viewCtrl
   * @param auth
   * @param alertCtrl
   * @param platform
   */
  constructor(public navParams: NavParams,
              public viewCtrl: ViewController,
              public auth: AuthService,
              private alertCtrl: AlertController,
              platform: Platform) {
    this.platform = platform;
  }

  /**
   * Check input parameters and set local data
   */
  ngOnInit() {
    this.trip = <Trip>this.navParams.get('trip');
    this.mileage_label = this.navParams.get('mileage_label');
    this.use_stop_contract = this.navParams.get('use_stop_contract');
    this.use_tickets = this.navParams.get('use_tickets');
    this.original_mileage_adjustment = this.trip.mileage_adjusted;
    this.pre_mileage_adjusted = (this.trip.mileage_adjusted ? this.trip.mileage_adjusted : 0);
    this.combined_mileage = parseFloat('' + this.trip.mileage_adjusted) + parseFloat('' + this.trip.mileage_original);
    this.submit_disabled = false;
    console.log('editTrip: init', this.trip);
  }

  /**
   * Check for null comment, call validator.
   * If validation passes, update to server, dismiss with updated trip and combined mileage
   */
  onSubmit() {
    console.log('business purpose ' + this.trip.business_purpose);
    let business_mileage = null;

    this.validateAll()
      .then(() => {
        this.auth.updateDailyTrip(this.trip.id, this.trip.business_purpose, this.pre_mileage_adjusted, this.trip.mileage_comment, this.trip.trip_date)
          .then(response => {
            console.log('update trip success', response);
            business_mileage = response['business_mileage'];
            this.trip.mileage_adjusted = this.pre_mileage_adjusted;
            this.viewCtrl.dismiss({trip: this.trip, business_mileage: business_mileage})
              .then(() => {
              });
          })
          .catch(err => {
            console.log('update trip error', err);
            this.viewCtrl.dismiss({trip: this.trip, business_mileage: business_mileage})
              .then(() => {
              });
          });
      })
      .catch(() => {
        // do nothing, they will dismiss an alert and correct input
      });
  }

  /**
   * Validate adjustment, business purpose and adjustment comment
   */
  validateAll() {
    // look for empty comment when required
    let adjustment_required = false;
    if ((this.pre_mileage_adjusted > 0 || this.pre_mileage_adjusted < 0)
      && (this.trip.mileage_comment == null || this.trip.mileage_comment == '')) {
      adjustment_required = true;
    }

    // reject when validation error occurs and show alert with explanation, else resolve all
    return new Promise<any>((resolve, reject) => {
      if (Math.abs(this.pre_mileage_adjusted ? this.pre_mileage_adjusted : 0) > 999.9) {
        let alert = this.alertCtrl.create({
          title: 'High Adjustment Detected',
          message: 'Please enter a mileage adjustment of magnitude less than 1000 miles',
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input input[name=mileage_adjusted]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (adjustment_required) {
        let alert = this.alertCtrl.create({
          title: 'Adjustment Reason Required',
          message: "Please enter a reason for adjusting the original mileage.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input input[name=mileage_comment]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (this.trip.mileage_comment && this.trip.mileage_comment.length > 199) {
        let alert = this.alertCtrl.create({
          title: 'Adjustment Reason Too Long',
          message: "Please enter a reason for adjusting the original mileage of less than 200 characters.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input input[name=mileage_comment]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (this.trip.business_purpose.length > 199) {
        let alert = this.alertCtrl.create({
          title: 'Business Purpose Too Long',
          message: "Please enter a business purpose of less than 200 characters.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input[name=business_purpose]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (this.use_stop_contract && this.use_tickets && (this.trip.business_purpose.match(/[^\d]/) || (this.trip.business_purpose.length !== 4 && this.trip.business_purpose.length !== 6))) {
        let alert = this.alertCtrl.create({
          title: 'Contract or Ticket Number Invalid',
          message: "Please enter a 4-digit contract number or 6-digit ticket number.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input[name=business_purpose]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (this.use_stop_contract && !this.use_tickets && (this.trip.business_purpose.match(/[^\d]/) || this.trip.business_purpose.length !== 4)) {
        let alert = this.alertCtrl.create({
          title: 'Contract Number Invalid',
          message: "Please enter a 4-digit contract number.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input[name=business_purpose]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (!this.trip.business_purpose || !this.trip.business_purpose.length || this.trip.business_purpose == "") {
        let alert = this.alertCtrl.create({
          title: 'Business Purpose Required',
          message: "Please enter a business purpose.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input[name=business_purpose]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else if (isNaN(this.pre_mileage_adjusted)) {
        let alert = this.alertCtrl.create({
          title: 'Adjustment Not a Number',
          message: 'Your input for mileage adjustment is not a number. Please correct this.',
          buttons: ['OK']
        });
        alert.present()
          .then(() => {
            reject();
          });
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input[name=mileage_adjusted]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else {
        resolve();
      }
    });
  }

  /**
   * just close up and return the original information
   */
  onCancel() {
    this.viewCtrl.dismiss({trip: this.trip, business_mileage: null})
      .then(() => {
      });
  }

  /**
   * Computed function to add original and adjusted mileage.
   * Complications arise when using characters for negative numbers ('-' character)
   */
  addThese() {
    this.submit_disabled = false;

    let n1 = this.trip.mileage_original;
    let n2 = '' + this.pre_mileage_adjusted;
    n2 = n2.replace('+', '');
    this.combined_mileage = n1;

    console.log('trying to add ' + n1 + ' ' + n2);

    if (n2.trim() == '-') {
      return this.combined_mileage;
    }

    if (parseFloat(n2)) {
      if (n2.includes('-')) {
        let pos = n2.trim().search('-');
        if (pos > 0) {
          /*let alert = this.alertCtrl.create({
            title: 'Adjustment Not a Number',
            message: 'Your input for mileage adjustment is not a number. Please correct this.',
            buttons: ['OK']
          });
          alert.present();*/
        } else {
          let x = n2.replace('-', '');
          console.log('now n2 = ' + x);
          this.combined_mileage -= parseFloat(x);
        }
      } else {
        this.combined_mileage += parseFloat(n2);
      }
    }

    return;
  }

  /**
   * Extra validation on the adjustment value only
   */
  checkAdjusted() {
    let title = '';
    let message = '';

    let test_val = '' + this.pre_mileage_adjusted;
    // console.log('check adjusted ' + test_val);
    if (Math.abs(this.pre_mileage_adjusted) < 0.1 && this.pre_mileage_adjusted != 0) {
      title = 'Adjustment Too Small';
      message = 'The magnitude of your mileage adjustment must be greater than 0.1. Please correct this.';
    } else if (test_val.trim().search(/^-?[0-9]*(\.[0-9][0-9]+)$/) !== -1) {
      title = 'Adjustment Has Too Many Decimal Places';
      message = 'Your input for mileage adjustment must be rounded to the nearest tenth. Please correct this.';
    } else if (test_val.trim().search(/^-?[0-9]*(\.[0-9]?)?$/) === -1 || this.pre_mileage_adjusted == "") {
      title = 'Adjustment Not a Number';
      message = 'Your input for mileage adjustment is not a number. Please correct this.';
    }

    if (title && message) {
      let alert = this.alertCtrl.create({
        title: title,
        message: message,
        buttons: ['OK']
      });
      alert.present()
        .then(() => {
        });

      this.submit_disabled = true;
    } else {
      this.submit_disabled = false;
    }
  }
}
