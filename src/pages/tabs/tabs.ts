import {Component, ViewChild} from '@angular/core';
import {DashboardPage} from "../dashboard/dashboard";
import {ReportsPage} from "../reports/reports";
import {AuthService} from "../../services/auth";
import {MileageEntryPage} from "../mileage-entry/mileage-entry";
import {MirouteStartPage} from "../miroute/miroute-start/miroute-start";
import {MirouteDrivingPage} from "../miroute/miroute-driving/miroute-driving";
import {MirouteStoppedPage} from "../miroute/miroute-stopped/miroute-stopped";
import {MirouteEndPage} from "../miroute/miroute-end/miroute-end";
import {MirouteReviewPage} from "../miroute/miroute-review/miroute-review";
import {UserInfo} from "../../models/userInfo";
import {Tabs} from "ionic-angular";
import {MirouteDayEndedPage} from "../miroute/miroute-day-ended/miroute-day-ended";

@Component({
  selector: 'page-tabs',
  templateUrl: 'tabs.html',
})
export class TabsPage {
  @ViewChild('mirouteTabs') tabRef: Tabs;
  dashboard = DashboardPage;
  reports = ReportsPage;
  review = MirouteReviewPage;
  mileageEntry = MileageEntryPage; // non-miroute user
  miroutePage: any = MirouteStartPage; // default
  userInfo: UserInfo;
  displayPoint2Point: boolean;
  displayMileageEntry: boolean;
  refreshSubscription;

  // tabChanged: EventEmitter<number> = new EventEmitter<number>();


  constructor(private authService: AuthService) {
    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      if (refresh) {
        this.setup()
          .then(() => {
            console.log('tabs: refresh setup done');
          })
          .catch(err => {
            console.log('tabs: failed to refresh setup', err);
          });
      }
    });

    this.setup()
      .then(() => {
        console.log('tabs: setup done');
      })
      .catch(err => {
        console.log('tabs: failed to setup', err);
      });
  }

  setup() {
    return new Promise<any>((resolve, reject) => {
      console.log('tabs: setup');
      this.authService.getUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('tabs: userInfo', userInfo);
          this.userInfo = userInfo;
          //const lastDrivingRecord = userInfo.lastDrivingRecord;
          //const miroute_page = userInfo.miroute_page;

          this.displayPoint2Point = userInfo.mileage_entry_method == 'Mi-Route';// && this.userInfo.miroute_page != 'day_ended';
          this.displayMileageEntry = userInfo.mileage_entry_method != 'Mi-Route';

          //let today = moment().add(2, 'day').format("YYYY-MM-DD");
          //let today = moment().format("YYYY-MM-DD");
          //let page = lastDrivingRecord.trip_date < today ? 'end_previous_day' : miroute_page;

          this.setPage()
            .then(() => {
              resolve();
            })
            .catch(err => {
              reject(err);
            });
        });
    });
  }

  setPage() {
    console.log('tabs: setting page to ' + this.userInfo.miroute_page);
    return new Promise<any>((resolve, reject) => {
      switch (this.userInfo.miroute_page) {
        case 'start_day':
          console.log('tabs: setting page to start_day');
          this.miroutePage = MirouteStartPage;
          break;
        case 'end_previous_day':
          this.miroutePage = MirouteEndPage;
          break;
        case 'end_day':
        case 'end-day':
          this.miroutePage = MirouteEndPage;
          break;
        case 'day_ended':
          this.miroutePage = MirouteDayEndedPage;
          break;
        case 'driving':
          this.miroutePage = MirouteDrivingPage;
          break;
        case 'where':
        case 'stopped':
          this.miroutePage = MirouteStoppedPage;
          break;
        case 'unknown':
          this.authService.logout();
          break;
        default:
          // error condition
          this.miroutePage = MirouteStartPage;
          break;
      }

      this.authService.apiLog('tabs set page to ' + this.userInfo.miroute_page)
        .then(() => {
          console.log('tabs: just called apiLog to set page ' + this.miroutePage);
        })
        .catch(err => {
          console.log('tabs: could not apiLog to set page ' + this.miroutePage, err);
        });

      resolve();
    });
  }

  /*tabSelected(tab: Tab) {
    console.log('tabs: changing tab to ' + tab.index);
    this.tabChanged.emit(tab.index);

    if (!this.userInfo) {
      return;
    }

    //let today = moment().add(2, 'day').format("YYYY-MM-DD");
    console.log('tabs: checking for new day');
    let today = moment().format("YYYY-MM-DD");
    if (this.userInfo.lastDrivingRecord.trip_date < today) {
      if (this.userInfo.lastDrivingRecord.ending_odometer == null || this.userInfo.lastDrivingRecord.ending_odometer == 0) {
        let page = 'end_previous_day';
        this.storageService.updateUserInfo({miroute_page: page})
          .then(() => {
            this.setPage(page);
          });
      } else {
        let page = 'start_day';
        this.storageService.updateUserInfo({miroute_page: page})
          .then(
            this.setPage(page);() => {
          });
      }
    }
  }*/
}
