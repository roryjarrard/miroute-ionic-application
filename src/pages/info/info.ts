import {Component} from '@angular/core';

@Component({
  selector: 'page-info',
  templateUrl: 'info.html',
})
export class InfoPage {
  display_answer_index: number;

  questions: string[] = [
    'How does Mi-Route calculate my business mileage?',
    'Are my personal trips and mileage tracked?',
    'Do I have to login to Mi-Route for every trip?',
    'Do I have to log in to Mi-Route every day?',
    'How do I start my day?',
    'What do I do when I arrive at my destination?',
'What if I am at a new location?',
    'How does my business day end?',
    'What if I forget to enter a stop?',
    'Need more help?'
  ];
  answers: string[] = [
    'Google Maps technology calculates the distance traveled to a business meeting. The first business trip of the day begins when you submit your starting odometer with current location, and the trip ends when you press the Arrive button. Mileage is recorded only after you press the Arrive button.',
    'Personal trips and personal mileage are NOT tracked. Google Maps technology calculates the business mileage from your trip origin to destination. Personal mileage is the difference in your beginning and ending odometer readings minus the business mileage for all your trips for the day.',
    "You only have to login to Mi-Route once. By checking the box 'remember me' on the login page, you will be able to access the Mi-Route platform without logging in again, as long as you do not actively log out of the application.",
    'You only have to login to Mi-Route once as long as you do not log out of the application.',
    "At the start of each day, log in to Mi-Route using your CarData credentials (if you aren't logged in already). It is suggested that you save your login credentials, by checking the box ‘remember me’ on the login screen, so you don’t have to log in to the site every day.<br><br>" +
    "Enter your vehicle’s odometer reading at the start of your first business trip for the day, select your location from the list of your saved locations, or create a new one, enter a business purpose, and press submit. The Driving page will appear. You can start driving to your destination.",
    "When you arrive at your destination, press the Arrive button. If the location is available on your list of saved Destinations, Mi-Route will provide the name of this location as your arrival location. If you haven’t been to the location before, you also have the opportunity to save this as a new Destination. Enter the name of the location in the text box and press Accept.<br><br>" +
      "Once the location is submitted you can either stay on this page, or depart on another trip. This location automatically becomes the Start of your next business trip.",
    "You can always create a new saved Destination by selecting 'Or create a new stop' in the Destination selection drop-down",
    "When you are at your final business destination for the day, press the End Day button (the checkered flag). On End Day page enter your final odometer reading for the day. If you want, take time to review your trips. If you need to edit the mileage, press the edit icon. In the event you need to add mileage insert a positive number into the mileage adjustment section. If you need to subtract mileage from your route, insert a negative number into the adjust mileage section. Enter a comment to describe why you needed to modify your mileage (for example: traffic on route). Comments are required. Once you end your trip for the day, Mi-Route will not be available until the next day.<br><br>" +
    "Review stops is always available on the third tab on your screen. Logout, setting and information (this page) through the menu icon in the top left of the application. To review data for multiple days go to: <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a>.",
    "If you forget to enter a stop, you can add a stop on the Review page, or online at <a href='https://www.cardataconsultants.com'>www.cardataconsultants.com</a>.",
"For additional questions or concerns about Mi-ROUTE go to https://www.cardataconsultants.com or contact your CarData Representative:<br><br>" +

"Megan Dean<br><br>" +
"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;mdean@cardataconsultants.com\n" +
"&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;1-303-434-3307"
  ];

  constructor() {
  }

  displayAnswer(index: number) {
    this.display_answer_index = index;
  }
}
