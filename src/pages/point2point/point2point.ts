import {Component} from '@angular/core';
import {Location} from "../../models/location";
import {LoadingController} from "ionic-angular";
import {Geolocation} from "@ionic-native/geolocation";

@Component({
  selector: 'page-point2point',
  templateUrl: 'point2point.html',
})
export class Point2pointPage {
  location: Location = {lat: 0, lng: 0};
  locationSet = false;

  constructor(private loadingCtrl: LoadingController, private geo: Geolocation) {
    //this.findCurrentLocation();

  }

  findCurrentLocation() {
    const loading = this.loadingCtrl.create({
      content: 'Finding location...'
    });
    loading.present();

    this.geo.getCurrentPosition()
      .then(location => {
        loading.dismiss();
        this.location.lat = location.coords.latitude;
        this.location.lng = location.coords.longitude;
        this.locationSet = true;
      })
      .catch(err => {
        loading.dismiss();
        console.log(err);
      });
  }
}
