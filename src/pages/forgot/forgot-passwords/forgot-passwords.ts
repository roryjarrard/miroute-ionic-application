import { Component } from '@angular/core';
import {AlertController, LoadingController, NavController} from "ionic-angular";
import {NgForm} from "@angular/forms";
import {AuthService} from "../../../services/auth";
import {TabsPage} from "../../tabs/tabs";

@Component({
  selector: 'page-forgot-passwords',
  templateUrl: 'forgot-passwords.html',
})
export class ForgotPasswordsPage {
  constructor(private navCtrl: NavController,
              private loadingCtrl: LoadingController,
              private alertCtrl: AlertController,
              private authService: AuthService) {
  }

  onSendPassword(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Sending new password...'
    });
    loading.present();
    if (form.value.password != form.value.password_confirm) {
      loading.dismiss();
      const nomatchAlert = this.alertCtrl.create({
        title: 'Password Mismatch',
        message: 'The passwords you entered do not match, please re-enter',
        buttons: ['OK']
      });
      nomatchAlert.present();
    } else {
      this.authService.submitNewPassword(form.value.password)
        .then(data => {
          loading.dismiss();

          const username = data['username'];
          const successAlert = this.alertCtrl.create({
            title: 'Password Updated',
            message: 'Your password has been successfully updated. Click below to sign in',
            buttons: ['Login']
          });
          successAlert.present();

          // auto login here
          successAlert.onDidDismiss(() => {
            this.authService.login(username, form.value.password)
              .then(data => {

                this.authService.updateAuth(true);
                this.navCtrl.setRoot(TabsPage);
              })
              .catch(err => {
                loading.dismiss();

                console.log('login: unable to login', err);

                const message = err['status'] == 0 ? 'Unable to contact login server' : 'Unable to validate username and password';
                const alert = this.alertCtrl.create({
                  title: 'Login Failed',
                  message: message,
                  buttons: ['OK']
                });
                alert.present();

                alert.onDidDismiss(() => {
                  this.authService.updateAuth(false);
                });
              })
          });
        })
        .catch(err => {
          loading.dismiss();

          const notfoundAlert = this.alertCtrl.create({
            title: 'Password Not Updated',
            message: err['error'].message,
            buttons: ['OK']
          });
          notfoundAlert.present();
        });
    }
  }

}
