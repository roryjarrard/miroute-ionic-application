import {Component} from '@angular/core';
import {NgForm} from "@angular/forms";
import {AuthService} from "../../../services/auth";
import {AlertController, LoadingController, NavController} from "ionic-angular";
import {ForgotPasswordsPage} from "../forgot-passwords/forgot-passwords";

@Component({
  selector: 'page-forgot-code',
  templateUrl: 'forgot-code.html',
})
export class ForgotCodePage {
  public show_reverification: boolean = false;

  constructor(private authService: AuthService, private navCtrl: NavController, private loadingCtrl: LoadingController, private alertCtrl: AlertController) {

  }

  onVerifyCode(form: NgForm) {
    const loading = this.loadingCtrl.create({
      content: 'Contacting server...'
    });
    loading.present();

    this.authService.validateCode(form.value.code)
      .then(data => {
        loading.dismiss();
        this.navCtrl.push(ForgotPasswordsPage);
      })
      .catch(err => {
        loading.dismiss();
        this.show_reverification = true;

        const message = err['error'].message;
        const alert = this.alertCtrl.create({
          title: 'Error Verifying Code',
          message: message,
          buttons: ['OK']
        });
        alert.present();
      });
  }
}
