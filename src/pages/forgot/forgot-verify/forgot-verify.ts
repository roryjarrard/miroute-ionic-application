import { Component } from '@angular/core';
import {AlertController, LoadingController, NavController} from "ionic-angular";
import {AuthService} from "../../../services/auth";
import {NgForm} from "@angular/forms";
//import {ForgotCodePage} from "../forgot-code/forgot-code";
import {LoginPage} from "../../login/login";

@Component({
  selector: 'page-forgot-verify',
  templateUrl: 'forgot-verify.html',
})
export class ForgotVerifyPage {

  constructor(private authService: AuthService,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController) {
  }

  onVerifyEmail(form: NgForm) {
    const email = form.value.email.trim();

    const loading = this.loadingCtrl.create({
      content: 'Contacting server...'
    });
    loading.present();

    this.authService.validateEmail(email)
      .then(() => {
        loading.dismiss();

        const alert = this.alertCtrl.create({
          title: 'New Password Sent',
          message: 'An email with your new password has been sent to the email you provided.',
          buttons: ['Return to Login']
        });
        alert.present();
        alert.onDidDismiss(() => {
          this.navCtrl.popTo(LoginPage);
        });
      })
      .catch(err => {
        loading.dismiss();

        const alert = this.alertCtrl.create({
          title: 'User Not Found',
          message: 'That email did not belong to any user',
          buttons: ['OK']
        });
        alert.present();
      });
  }
}
