import {Component} from '@angular/core';
import {UserInfo} from "../../models/userInfo";
import {Trip} from "../../models/trip";
import {AuthService} from "../../services/auth";
import {AlertController, NavController} from "ionic-angular";
import moment from "moment";

@Component({
  selector: 'page-dashboard',
  templateUrl: 'dashboard.html',
})
export class DashboardPage {
  userInfo: UserInfo;
  street_light_id: number;
  street_light_color: string;
  trips: Trip[] = [];
  last_entry_date: string;
  last_entry_day: number;
  last_entry_month: string;
  last_entry_year: number;
  business_mileage: number = 0;
  driverStatusItems: Object[] = [];
  day_to_end: string;
  setting_up: boolean = false;
  id;
  refreshSubscription;
  logoutSubscription;
  full_mileage_units_name: string = '';

  constructor(private authService: AuthService,
              private alertCtrl: AlertController,
              private navCtrl: NavController) {
    this.id = Math.random() * 50;

  }

  ionViewWillEnter() {
    console.log('dashboard: calling setup from enter, id ' + this.id);
    if (!this.setting_up) {
      this.setup()
        .then(() => {
          this.setting_up = false;
          console.log('dashboard: setup done');
        })
        .catch(err => {
          this.setting_up = false;
          console.log('dashboard: failed to setup', err);
        });
    }

    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      if (refresh && !this.setting_up) {
        console.log('dashboard: calling setup from listener, id ' + this.id);
        this.setup()
          .then(() => {
            this.setting_up = false;
            console.log('dashboard: refresh then setup done');
          })
          .catch(err => {
            this.setting_up = false;
            console.log('dashboard: failed to refresh data', err);
          });
      }
    });

    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    });
  }

  ionViewWillLeave() {
    console.log('dashboard: leaving');
    this.userInfo = <UserInfo>{};
    this.refreshSubscription.unsubscribe();
  }

  setup() {
    console.log('starting setup');
    this.setting_up = true;
    return new Promise((resolve, reject) => {
      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('dashboard: userInfo', userInfo);
          this.userInfo = userInfo;
          this.street_light_id = userInfo.street_light_id;
          this.street_light_color = userInfo.street_light_color;
          this.last_entry_date = userInfo.last_entry_date;
          this.last_entry_day = moment(this.last_entry_date).date();
          this.last_entry_month = moment(this.last_entry_date).format('MMM');
          this.last_entry_year = moment(this.last_entry_date).year() < moment().year() ? moment(this.last_entry_date).year() : null;
          this.business_mileage = userInfo.lastDrivingRecord.business_mileage ? userInfo.lastDrivingRecord.business_mileage : 0;
          this.trips = userInfo.trips;
          this.driverStatusItems = userInfo.driverStatusItems;
          this.full_mileage_units_name = userInfo.country == 'US' ? 'Miles' : 'Kilometers';
          this.day_to_end = moment(this.userInfo.last_entry_date).format('dddd, MMMM Do YYYY');

          if (userInfo.miroute_page == 'end_previous_day') {
            this.endPreviousDay();
          }
          resolve();
        })
        .catch(err => {
          console.log('dashboard: could not get userInfo from auth');
          reject(err);
        });
    });
  }

  endPreviousDay() {
    const alert = this.alertCtrl.create({
      title: 'End Your Previous Day',
      message: "Please end your day from " + this.day_to_end,
      buttons: ['OK']
    });
    alert.present();

    alert.onDidDismiss(() => {
      this.refreshSubscription.unsubscribe();
      setTimeout(() => {
        this.navCtrl.parent.select(1);
      }, 300);
    });
  }
}
