import {Component} from '@angular/core';
import {AlertController, NavParams, ViewController} from "ionic-angular";
import {NgForm} from "@angular/forms";
import {Stop} from "../../../models/stop";
import {AuthService} from "../../../services/auth";

@Component({
  selector: 'page-miroute-new-stop',
  templateUrl: 'miroute-new-stop.html',
})
export class MirouteNewStopPage {
  newStop: Stop = <Stop>{};
  displayAddress = false;
  inputLocation: any = null;

  stopName: string = '';
  stop_submitted: boolean = false;
  stopCountry: string = '';
  user_id: number;

  constructor(params: NavParams,
              private viewCtrl: ViewController,
              private alertCtrl: AlertController,
              private authService: AuthService) {
    console.log('new stop params', params);
    this.inputLocation = params.get('geo');
    this.newStop.latitude = params.get('latitude');
    this.newStop.longitude = params.get('longitude');
    this.user_id = params.get('user_id');
    console.log('set user_id to ' + this.user_id + ' param ' + params.get('user_id'));

    console.log(this.inputLocation);
    let components = this.inputLocation.address_components;

    let stopStreetNumber = components[0].long_name;
    let stopStreetName = components[1].long_name;
    this.newStop.street = stopStreetNumber + ' ' + stopStreetName;

    for (let c of components) {
      let types = c.types;

      if (types.indexOf('locality') != -1) {
        this.newStop.city = c.long_name;
      }
      if (types.indexOf('administrative_area_level_1') != -1) {
        this.newStop.state = c.short_name;
      }
      if (types.indexOf('country') != -1) {
        this.stopCountry = c.short_name; // we don't use this, only for display in the form
      }
      if (types.indexOf('postal_code') != -1) {
        this.newStop.zip_postal = c.long_name;
      }
    }

    this.displayAddress = true;
  }

  onSubmit(form: NgForm) {


    if (this.stop_submitted) {
      return;
    }
    this.stop_submitted = true;

    // TODO: normal validation wasn't working on submit button
    console.log(form.value);

    if (form.value.stopName == null || form.value.stopName == '' || form.value.stopName.length < 3) {
      const alert = this.alertCtrl.create({
        title: 'Stop Name Missing',
        message: "Please enter a name for this stop of at least 3 characters.",
        buttons: ['OK']
      });
      alert.present();

      alert.onDidDismiss(() => {
        this.stop_submitted = false;
        const input: any = document.querySelector('input[name=stopName]');
        setTimeout(() => {
          input.focus();
        }, 300);
      });
    } else if (form.value.stopName.length > 79) {
      const alert = this.alertCtrl.create({
        title: 'Stop Name Too Long',
        message: "Please enter a name for this stop of less than 80 characters.",
        buttons: ['OK']
      });
      alert.present();

      alert.onDidDismiss(() => {
        this.stop_submitted = false;
        const input: any = document.querySelector('input[name=stopName]');
        setTimeout(() => {
          input.focus();
        }, 300);
      });
    } else {
      this.newStop.name = form.value.stopName;
      console.log('new stop', this.newStop, 'user_id', this.user_id);
      this.authService.submitNewStop(this.newStop, this.user_id)
        .then(data => {
          console.log('new stop: submit success', data);
          this.viewCtrl.dismiss({'newStop': data['new stop']})
        })
        .catch(err => {
          this.stop_submitted = false; // don't know if we should do this or not, can't get this error
          console.log('new stop: submit fail', err);
        });

    }
  }

  onCancel() {
    this.viewCtrl.dismiss({'action': 'cancelled'});
  }
}
