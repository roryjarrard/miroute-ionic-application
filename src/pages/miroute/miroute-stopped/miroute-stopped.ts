import {Component, ViewChild} from '@angular/core';
import {Stop} from "../../../models/stop";
import {Location} from "../../../models/location";
import {AlertController, LoadingController, ModalController, NavController, Select} from "ionic-angular";
import {Geolocation} from "@ionic-native/geolocation";
import {AuthService} from "../../../services/auth";
import {ApiService} from "../../../services/api";
import {MirouteDrivingPage} from "../miroute-driving/miroute-driving";
import {TabsPage} from "../../tabs/tabs";
import {NgForm} from "@angular/forms";
import {MirouteNewStopPage} from "../miroute-new-stop/miroute-new-stop";
import {UserInfo} from "../../../models/userInfo";
import {MirouteEndPage} from "../miroute-end/miroute-end";
import {MirouteReviewPage} from "../miroute-review/miroute-review";
import {MirouteStartPage} from "../miroute-start/miroute-start";
import {MirouteDayEndedPage} from "../miroute-day-ended/miroute-day-ended";

declare var google;

@Component({
  selector: 'page-miroute-stopped',
  templateUrl: 'miroute-stopped.html',
})
export class MirouteStoppedPage {
  //region DATA MEMBERS
  id;                                           // id for this component, for debug purposes
  userInfo: UserInfo;                           // model instance, data from server
  currentLocation: Location = {lat: 0, lng: 0}; // for map without stop, show current location
  location: Location = {lat: 0, lng: 0};        // model attached to chosen location in select
  stops: Stop[] = [];                           // used to hold nearby stops
  stopType: string;                             // chosen stop type (driver, company), sent to server
  stopName: string;                             // chosen stop name, sent to server
  selected_contracts: number[] = [];            // all contracts associated to nearby stops, from server
  selected_tickets: number[] = [];              // all tickets associated to nearby stops, from server
  use_stop_contract: number = 0;                // company selection
  use_tickets: number = 0;                      // certain drivers in stop_contract companies
  departed = false;                             // used to switch or disable button
  disable_submit_button = false;                // disable to keep from double submit
  got_stops_from_server = false;                // used to disable button when request sent to server
  locationIsSet = false;                        // trigger to display the map
  showOtherContract = false;                    // used to show free-text other contract input
  stopIsSelected = false;                       // trigger to display stop on map
  stopIsSubmitted = false;                      // let HTML know if 'where' or 'stopped' state
  refreshSubscription;                          // attached to auth refreshDataEmitter subscription
  logoutSubscription;                           // attached to auth unsubscribeAllEmitter subscription
  @ViewChild('stop_select') stopSelect: Select; // attached to select element in html
  //endregion

  /**
   * Constructor only sets id for this instance
   *
   * @param geo
   * @param loadingCtrl
   * @param navCtrl
   * @param alertCtrl
   * @param authService
   * @param modalCtrl
   * @param api
   */
  constructor(private geo: Geolocation,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private alertCtrl: AlertController,
              private authService: AuthService,
              private modalCtrl: ModalController,
              private api: ApiService) {

    this.id = Math.random() * 10;
    console.log('\n\n------- CONSTRUCTING MIROUTE-STOPPED.TS -------');
    console.log('ID: ' + this.id);
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Call setup() every time we enter page,
   * subscribe to auth emitters
   */
  ionViewWillEnter() {
    this.setup()
      .then(() => {
        console.log('stopped: setup done');
      })
      .catch(err => {
        console.log('stopped: failed to setup', err);
      });

    console.log('MIROUTE-STOPPED.TS SUBSCRIBING');
    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      console.log('MIROUTE-STOPPED.TS ID ' + this.id + ' refreshing data');
      if (refresh) {
        this.setup()
          .then(() => {
            console.log('MIROUTE-STOPPED.TS ID ' + this.id + 'refresh then setup done');
          })
          .catch(err => {
            console.log('stopped: failed to refresh data', err);
          });
      }
    });

    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    })
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Unsubscribe to auth emitter, wipe out local data
   */
  ionViewWillLeave() {
    console.log('\n\n------- MIROUTE-STOPPED ION VIEW WILL LEAVE');
    console.log('MIROUTE-STOPPED ID: ' + this.id + ' UNSUBSCRIBING \n\n');
    this.refreshSubscription.unsubscribe();
    this.userInfo = <UserInfo>{};
  }

  /**
   * Retrieve user data from server, set local data.
   * Determine if page is valid for current state.
   * Determine if we are in state 'where' (stop needs to be selected) or 'stopped' (stop already selected).
   * Find current location (lat&lng) and stops near that location from server.
   */
  setup() {
    return new Promise<any>((resolve, reject) => {

      // determine if we need to locate or if we have already submitted a stop
      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('stopped: userInfo', userInfo);
          this.userInfo = userInfo;

          // make sure this page is what userInfo thinks
          this.testPage()
            .then(() => {
              // this means miroute page is either 'stopped' or 'where'

              // are we using stop contract entry method?
              this.use_stop_contract = this.userInfo.use_stop_contract;
              this.use_tickets = this.userInfo.use_tickets;

              // determine which phase we are in (stopped or where)
              console.log('miroute page', this.userInfo.miroute_page);
              if (this.userInfo.miroute_page.trim() != 'stopped' && this.userInfo.miroute_page.trim() != 'where') {
                this.userInfo.miroute_page = 'where';
              }
              if (this.userInfo.miroute_page.trim() == 'stopped') {
                console.log('stopped: last page was stopped, showing last stop');
                this.stopIsSubmitted = true;
                this.location = <Location>{
                  lat: this.userInfo.trips[this.userInfo.trips.length - 1].stop.latitude,
                  lng: this.userInfo.trips[this.userInfo.trips.length - 1].stop.longitude
                };
                this.stopName = this.userInfo.trips[this.userInfo.trips.length - 1].stop.name;
                this.locationIsSet = true;
                resolve();
              } else if (this.userInfo.miroute_page.trim() == 'where') {
                console.log('stopped: last page was where, getting current location');
                this.findCurrentLocation()
                  .then(() => {
                    this.getStopsFromServer()
                      .then(() => {
                        resolve();
                      })
                      .catch(err => {
                        console.log('stopped: could not get stops in setup', err);
                        reject(err);
                      });
                  })
                  .catch(err => {
                    console.log('stopped: could not find current location');
                    reject(err);
                  });
              } else {
                reject('stopped: page in storage is invalid');
              }
            })
            .catch(() => {
              // this means page was different and nav has already moved us
            });
        })
        .catch(err => {
          console.log('stopped: could not fetch userInfo');
          reject(err);
        });
    });
  }

  /**
   * Page from api must be 'where' or 'stopped'
   * 'start_day' or 'end_previous_day' need to be forced
   */
  testPage() {
    console.log('stopped: testPage ' + this.userInfo.miroute_page);
    return new Promise<any>((resolve, reject) => {
      switch (this.userInfo.miroute_page) {
        case 'start_day':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteStartPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'end_previous_day':
        case 'end_day':
        case 'end-day':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteEndPage)
            .then(() => {this.refreshSubscription.unsubscribe();});
          reject();
          break;
        case 'day_ended':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteDayEndedPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'driving':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteDrivingPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'unknown':
          this.authService.logout()
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'where':
        case 'stopped':
        default:
          resolve();
      }
    });
  }

  /**
   * Find stops within certain radius of current location (determined in setup())
   */
  getStopsFromServer() {
    console.log('getting stops');
    return new Promise<any>((resolve, reject) => {
      this.authService.getNearbyStops(this.location.lat, this.location.lng)
        .then((stops: Stop[]) => {
          console.log('start: nearby stops', stops);
          this.stops = stops;
          this.got_stops_from_server = true;
          resolve();
        })
        .catch(err => {
          console.log('start: error getting nearby stops', err);
          reject(err);
        });
    });
  }

  /**
   * Initial action after submitting stop and business purpose.
   * Validate data. Short circuit with alert error. Submit if no error.
   * Update state from 'where' to 'stopped'.
   *
   * @param form
   */
  onSubmit(form: NgForm) {
    // test if form is already submitted and waiting for response, don't allow 2nd submission
    if (this.disable_submit_button) {
      return;
    }

    //console.log('start form', form.value);

    // set button to disabled now
    this.disable_submit_button = true;

    // set local stop to be submitted
    let selectedStop = this.stops[this.stopSelect.value];

    // use local business_purpose to determine if form business_purpose is text or a contract/ticket number
    let business_purpose: string;
    if (this.use_stop_contract) {
      business_purpose = form.value.contract_number == 'Other' ? form.value.other_contract : form.value.contract_number;
      /**
       * Cast business purpose to a string, on the off chance it is not one. This is required
       * for the regex validation below
       */
      business_purpose = String(business_purpose);
    } else {
      business_purpose = form.value.business_purpose;
    }

    //region validate the business purpose here
    console.log('I see business purpose of ' + business_purpose);
    if (business_purpose.length > 199) {
      let alert = this.alertCtrl.create({
        title: 'Business Purpose Too Long',
        message: "Please enter a business purpose of less than 200 characters.",
        buttons: ['OK']
      });
      alert.present()
        .then(() => {});
      alert.onDidDismiss(() => {
        const input: any = document.querySelector('ion-input input[name=business_purpose]');
        setTimeout(() => {
          input.focus();
        }, 300);
      });
      this.disable_submit_button = false;
      return;
    } else if (this.use_stop_contract && (business_purpose == null || business_purpose == '')) {
      const contractAlert = this.alertCtrl.create({
        title: "No Contract Provided",
        message: "Please select a contract from the drop-down list, or select \"Other\" and enter in an alternate contract in the text field provided.",
        buttons: ['OK']
      });
      contractAlert.present()
        .then(() => {});

      this.disable_submit_button = false;
      return;
    } else if (this.use_stop_contract && !this.use_tickets && (business_purpose.match(/[^\d]/) || business_purpose.length !== 4)) {
      const contractAlert = this.alertCtrl.create({
        title: "Not a Valid Contract Number",
        message: "Please provide a contract number of 4 numeric digits only.",
        buttons: ['OK']
      });
      contractAlert.present()
        .then(() => {});

      this.disable_submit_button = false;
      return;
    } else if (this.use_stop_contract && this.use_tickets && (business_purpose.match(/[^\d]/) || (business_purpose.length !== 6 && business_purpose.length !== 4))) {
      const contractAlert = this.alertCtrl.create({
        title: "Not a Valid Ticket or Contract Number",
        message: "Please provide a 6-digit ticket number or 4-digit contract number.",
        buttons: ['OK']
      });
      contractAlert.present()
        .then(() => {});

      this.disable_submit_button = false;
      return;
    }
    //endregion

    // validation passed, submit stop
    this.api.fetch('atStop', {
      user_id: this.userInfo.user_id,
      business_purpose: business_purpose,
      event: 'stopped',
      destination: selectedStop['name'],
      saved_stop_id: selectedStop['id'],
      stopType: selectedStop['type'],
      country: this.userInfo.country
    }, true)
      .subscribe(
        data => {
          // success
          console.log('stopped: after submit stop', data);
          this.stopName = selectedStop['name'];

          // let html know we are in 'stopped', not 'where' state
          this.stopIsSubmitted = true;

          // now re-enable the submit button
          this.disable_submit_button = false;
        },
        err => {
          console.log('stopped: submit stop fail', err.error);

          // it might be possible to have a stale page, and have ended your day online.
          // this is only valid in Fusion. Legacy will not allow you to end online.
          if (err.error.message == 'day_ended') {
            let alert = this.alertCtrl.create({
              title: 'Day Already Ended',
              message: "It appears you already ended your day on Fusion Online. Do you want to submit this stop or end your day?",
              buttons: [
                {
                  text: 'Submit Stop',
                  handler: () => {
                    this.resumeDriving();
                  }
                },
                {
                  text: 'End Day',
                  handler: () => {
                    this.endDay();
                  }
                }
              ]
            });
            alert.present()
              .then(() => {});
          }
        }
      );
  }

  /**
   * Unsubscribe to auth emitter, only show Review page
   */
  endDay() {
    this.navCtrl.push(MirouteReviewPage)
      .then(() => {
        this.refreshSubscription.unsubscribe();
      });
  }

  /**
   * Un-end your day, resume driving and adding trips for current day.
   * Only valid in Fusion.
   */
  resumeDriving() {
    this.authService.resumeDriving(this.userInfo.lastDrivingRecord.trip_date)
      .then(response => {
        console.log('userinfo AFTER resume', response['userInfo']);
        this.userInfo = response['userInfo'];
        let element: HTMLElement = document.getElementById('submitStopButton');
        element.click();
      });
  }

  /**
   * After pull-down to refresh, call setup() again
   *
   * @param refresher
   */
  doRefresh(refresher) {
    this.setup()
      .then(() => {
        console.log('stopped: refresh then setup done');
        refresher.complete();
      })
      .catch(err => {
        console.log('stopped: failed to refresh data', err);
        refresher.complete();
      });
  }

  /**
   * Use GPS to get current location, set local variables
   */
  findCurrentLocation() {
    return new Promise((resolve, reject) => {
      const loading = this.loadingCtrl.create({
        content: 'Finding location...'
      });
      loading.present()
        .then(() => {});

      let options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };

      this.geo.getCurrentPosition(options)
        .then(location => {
          loading.dismiss()
            .then(() => {});

          console.log('location', location);

          // set both currentLocation and location
          this.currentLocation.lat = location.coords.latitude;
          this.currentLocation.lng = location.coords.longitude;

          this.location.lat = location.coords.latitude;
          this.location.lng = location.coords.longitude;
          console.log(this.location);

          // allow html to know we have a location and can show map
          this.locationIsSet = true;
          resolve();
        })
        .catch(err => {
          loading.dismiss()
            .then(() => {});

          console.log(err);
          reject();
        });
    });
  }

  /**
   * Use stop selected to determine if we need add contracts or tickets to dropdown
   * @param event
   */
  onSelectStop(event) {
    console.log('changed', event);
    if (event == 'new') {
      this.stopIsSelected = false;
      this.createNewStop();
    } else if (event >= 0) {
      this.locationIsSet = false;
      this.location = {lat: this.stops[event]['latitude'], lng: this.stops[event]['longitude']};
      this.stopType = this.stops[event]['company_stop_id'] == null ? 'driver' : 'company';
      this.stopName = this.stops[event]['name'];

      if (this.use_stop_contract) {
        this.selected_contracts = this.stops[event]['contracts'];
      }

      if (this.use_tickets) {
        this.selected_tickets = this.stops[event]['tickets'];
      }

      // location is set because it is selected stop lat&lng
      this.locationIsSet = true;

      // let html know stop has been selected
      this.stopIsSelected = true;
    } else if (event == -1) {
      this.stopIsSelected = false;
    } else {
      this.stopIsSelected = false;
    }
  }

  /**
   * Use current location to create a new stop.
   * Uses the MirouteNewStopPage modal.
   * Call handler if unable to geocode current location.
   */
  createNewStop() {
    let me = this;
    console.log('creating a new stop', this.currentLocation);
    let google_maps_geocoder = new google.maps.Geocoder();
    const latlng = new google.maps.LatLng(this.currentLocation.lat, this.currentLocation.lng);

    console.log('geocoding now', latlng);
    google_maps_geocoder.geocode(
      {location: latlng},
      function (results, status) {
        console.log(results, status);
        if (status != 'OK') {
          me.handleBadGeocodingResponse(status);
        } else {
          let newStopModal = me.modalCtrl.create(MirouteNewStopPage, {
            geo: results[0],
            latitude: me.currentLocation.lat,
            longitude: me.currentLocation.lng,
            user_id: me.userInfo.user_id
          });
          console.log('presenting modal', newStopModal);
          newStopModal.present()
            .then(() => {});

          newStopModal.onDidDismiss((data: any) => me.modalDismissed(data));
        }
      }
    );
  }

  /**
   * After creating a new stop, add to our stop collection.
   *
   * @param data
   */
  modalDismissed(data: any) {
    console.log('modal dismissed', data, this.stopSelect);
    if (data['newStop']) {
      this.stops.push(data['newStop']);
      this.stopSelect.setValue(this.stops.length - 1);
      this.stopType = 'driver'; // only driver after modal
      this.stopName = data['newStop']['name'];
    }
    else {
      this.stopSelect.setValue(-1);
    }
  }

  /**
   * No state of geocoding error has an implementation.
   * TODO: add alerts for each state
   * @param status
   */
  handleBadGeocodingResponse(status) {
    switch (status) {
      case 'ZERO_RESULTS':
        /**
         * indicates that the geocode was successful but returned no results. This may occur if the geocoder was passed a non-existent address
         */
        break;
      case 'OVER_DAILY_LIMIT':
        /**
         * indicates any of the following
         * - api key missing or invalid
         * - billing not enabled for account
         * - self-imposed usage cap has been exceeded
         * - provided method of payment is no longer valid
         */
        break;
      case 'OVER_QUERY_LIMIT':
        /**
         * indicates that you are over your quota
         */
        break;
      case 'REQUEST_DENIED':
        /**
         * indicates that your request was denied
         */
        break;
      case 'INVALID_REQUEST':
        /**
         * generally indicates that the query (address, components or latlng) is missing
         */
        break;
      case 'UNKNOWN_ERROR':
        /**
         * indicates that the request could not be processed due to a server error. The request may succeed if you try again
         */
        break;
      default:
        break;
    }

    const alert = this.alertCtrl.create({
      title: 'Unable To Create New Location',
      message: "We were unable to create a new location at this time. Please try again shortly, or contact your CSA if the problem persists.",
      buttons: ['OK']
    });
    alert.present()
      .then(() => {});

    alert.onDidDismiss(() => {
      this.navCtrl.setRoot(TabsPage)
        .then(() => {
          this.refreshSubscription.unsubscribe();
        });
    });
  }

  /**
   * If you selected "Other" in your contract selection, set boolean so other text input will show.
   *
   * @param event
   */
  onSelectContract(event: any) {
    this.showOtherContract = event == 'Other';
  }

  /**
   * When departing, set state to 'driving' and go to driving page.
   */
  onDepart() {
    this.departed = true;

    this.authService.updateLastState({
      user_id: this.userInfo.user_id,
      last_state: 'driving'
    })
      .then((userInfo: UserInfo) => {
        this.userInfo = userInfo;
        console.log('stopped: miroute page was set in storage, redirecting', userInfo);

        this.navCtrl.setRoot(MirouteDrivingPage)
          .then(() => {
            this.refreshSubscription.unsubscribe();
          });
      })
      .catch(err => {
        console.log('stopped: unable to resolve update to last state', err);

        // just go to driving anyway
        this.navCtrl.setRoot(MirouteDrivingPage)
          .then(() => {
            this.refreshSubscription.unsubscribe();
          });
      });
  }

  /**
   * When clicking the end day button (checkered flag), go to that page.
   * Do not set in state, and push on stack so driver can return to this page if desired (cancel end action).
   */
  onEndDay() {
    console.log('Ending Day');
    this.navCtrl.push(MirouteEndPage)
      .then(() => {
        this.refreshSubscription.unsubscribe();
      });
  }
}
