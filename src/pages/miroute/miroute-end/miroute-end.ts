import {Component} from '@angular/core';
import {AuthService} from "../../../services/auth";
import {AlertController, LoadingController, ModalController, NavController} from "ionic-angular";
import {NgForm} from "@angular/forms";
import {Trip} from "../../../models/trip";
import {MirouteStartPage} from "../miroute-start/miroute-start";
import {UserInfo} from "../../../models/userInfo";
import {EditTripModalPage} from "../../partials/edit-trip-modal/edit-trip-modal";
import moment from 'moment';
import {MirouteDayEndedPage} from "../miroute-day-ended/miroute-day-ended";

@Component({
  selector: 'page-miroute-end',
  templateUrl: 'miroute-end.html',
})
export class MirouteEndPage {
  //region DATA MEMBERS
  id;                                         // id of this instance, for debug purpose only
  userInfo: UserInfo;                         // model for user data
  trips: Trip[] = [];                         // trips to show route driven today
  last_entry_date: string;                    // used for date calendar display
  last_entry_day: number;                     // used for date calendar display
  last_entry_month: string;                   // used for date calendar display
  last_entry_year: number;                    // used for date calendar display
  business_mileage: number = 0;               // sum for display
  page: string = '';                          // end_day or end_previous_day
  display_route_map: boolean = false;         // use partial to display route for today's stops
  display_agm_map: boolean = false;           // agm map shows one stop better (not route)
  shownGroup;                                 // used to show content when a stop is toggled
  edit_mode = false;                          // display toggle, opens the edit trip modal
  can_edit_mileage = false;                   // if day was in the past, you may not edit
  day_to_end: string;                         // show specific day when in the past
  refreshSubscription;                        // attached to auth refreshDataEmitter subscription
  logoutSubscription;                         // attached to auth unsubscribeAllEmitter subscription
  full_mileage_units_name: string = '';       // used to display mileage or kilometers based on country
  use_stop_contract: number = 0;              // company selection
  use_tickets: number = 0;                    // certain drivers in stop_contract companies
  last_starting_odometer: number;             // aide to driver when ending previous day
  //endregion

  /**
   * Constructor sets id only
   *
   * @param authService
   * @param alertCtrl
   * @param loadingCtrl
   * @param navCtrl
   * @param modalCtrl
   */
  constructor(private authService: AuthService,
              private alertCtrl: AlertController,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private modalCtrl: ModalController) {
    this.id = Math.random() * 10;
    console.log('\n\n------- CONSTRUCTING MIROUTE-END.TS -------');
    console.log('ID: ' + this.id);
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Call setup(), subscribe to auth emitters.
   */
  ionViewWillEnter() {
    this.setup()
      .then(() => {
        console.log('end: setup done');
      })
      .catch(err => {
        console.log('end: failed to setup', err);
      });

    console.log('MIROUTE-END.TS SUBSCRIBING');
    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      console.log('MIROUTE-END.TS ID ' + this.id + ' refreshing data');

      if (refresh) {
        this.setup()
          .then(() => {
            console.log('MIROUTE-END.TS ID ' + this.id + ' refresh then setup done');
          })
          .catch(err => {
            console.log('end: failed to refresh data', err);
          });
      }
    });

    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    })
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Unsubscribe from auth emitters, wipe out local user info
   */
  ionViewWillLeave() {
    console.log('MIROUTE-END ID: ' + this.id + ' UNSUBSCRIBING \n\n');
    this.refreshSubscription.unsubscribe();
    this.userInfo = <UserInfo>{};
  }

  /**
   * Retrieve current user info from server. Test if need to end previous day.
   * Set local values. Show route map if more than 1 stop, agm map if only 1 stop, no map if no stops.
   */
  setup() {
    this.day_to_end = moment().format('dddd, MMMM Do YYYY');
    return new Promise<any>((resolve, reject) => {
      // always get updated userInfo
      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('end: userInfo', userInfo);
          this.userInfo = userInfo;
          this.last_entry_date = userInfo.last_entry_date;
          this.last_entry_day = moment(this.last_entry_date).date();
          this.last_entry_month = moment(this.last_entry_date).format('MMM');
          this.last_entry_year = moment(this.last_entry_date).year() < moment().year() ? moment(this.last_entry_date).year() : null;
          this.day_to_end = moment(this.last_entry_date).format('dddd, MMMM Do YYYY');
          this.last_starting_odometer = userInfo.lastDrivingRecord['starting_odometer'];
          this.business_mileage = userInfo.lastDrivingRecord['business_mileage'] ? userInfo.lastDrivingRecord['business_mileage'] : 0;
          this.page = userInfo.miroute_page;
          this.full_mileage_units_name = userInfo.country == 'US' ? 'Miles' : 'Kilometers';
          this.can_edit_mileage = moment().format('YYYY-MM-DD') === moment(this.userInfo.lastDrivingRecord.trip_date).format('YYYY-MM-DD');
          this.use_stop_contract = this.userInfo.use_stop_contract;
          this.use_tickets = this.userInfo.use_tickets;
          this.trips = <Trip[]>userInfo.trips;
          console.log('end: trips', this.trips);
          if (this.trips && this.trips.length > 1) {
            this.display_route_map = true;
          } else if (this.trips && this.trips.length == 1) {
            this.display_agm_map = true;
          }

          resolve();
        })
        .catch(err => {
          console.log('end: unable to get user data from auth service');
          reject(err);
        });
    });
  }

  /**
   * Toggle which trip is being shown for display and edit purposes
   *
   * @param group
   */
  toggleGroup(group) {
    if (this.isShownGroup(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  /**
   * If this group is shown used to display elements in the html
   *
   * @param group
   */
  isShownGroup(group) {
    return this.shownGroup == group;
  }

  /**
   * Initial form submit method. Validation here.
   * If validation passes, calls handleOdometerSubmit()
   *
   * @param {NgForm} form
   */
  onSubmit(form: NgForm) {
    const diff = form.value.ending_odometer - this.last_starting_odometer;
    const low = form.value.ending_odometer < Math.floor(this.last_starting_odometer + this.business_mileage);
    console.log('diff', diff);

    // handle a high mileage situation before submitting to server
    if (form.value.ending_odometer > 999999) {
      this.handleHighOdometer()
        .then(() => {
          const input: any = document.querySelector('ion-input input');
          //console.log(input);
          setTimeout(() => {
            input.focus();
          }, 300);
        });
    } else if (!parseInt('' + form.value.ending_odometer)) {
      this.handleOdometer('empty')
        .then(() => {
          const input: any = document.querySelector('ion-input input');
          //console.log(input);
          setTimeout(() => {
            input.focus();
          }, 300);
        });
    } else if (('' + form.value.ending_odometer).search(/^[0-9]+$/) === -1) {
      this.handleOdometer('invalid')
        .then(() => {
          const input: any = document.querySelector('ion-input input');
          //console.log(input);
          setTimeout(() => {
            input.focus();
          }, 300);
        });
    } else if (diff > 500) {
      this.handleHighMileage()
        .then(result => {
          console.log('got this from handler', result);

          if (!result) {
            const input: any = document.querySelector('ion-input input');
            //console.log(input);
            setTimeout(() => {
              input.focus();
            }, 300);
          } else {
            this.handleOdometerSubmit(form.value.ending_odometer);
          }
        });
    } else if (low) {
      this.handleLowOdometer()
        .then(result => {
          console.log('got this from handler', result);

          if (!result) {
            const input: any = document.querySelector('ion-input input');
            //console.log(input);
            setTimeout(() => {
              input.focus();
            }, 300);
          } else {
            this.handleOdometerSubmit(form.value.ending_odometer);
          }
        });
    } else {
      this.handleOdometerSubmit(form.value.ending_odometer);
    }
  }

  /**
   * Submit the odometer to server here
   *
   * @param {number} odometer
   */
  handleOdometerSubmit(odometer: number) {
    console.log('handler got ' + odometer);
    const loading = this.loadingCtrl.create({
      content: 'Submitting odometer...'
    });
    loading.present()
      .then(() => {});

    this.authService.submitEndingOdometer(this.last_entry_date, odometer, this.page)
      .then((userInfo: UserInfo) => {
        loading.dismiss()
          .then(() => {});
        console.log('success submitting odometer');
        this.userInfo = userInfo;
        let last_state = userInfo.lastDrivingRecord['last_state'];
        let page = last_state == 'start_day' ? MirouteStartPage : MirouteDayEndedPage;

        this.navCtrl.setRoot(page)
          .then(() => {
            this.refreshSubscription.unsubscribe();
          });
      })
      .catch(err => {
        loading.dismiss()
          .then(() => {});

        const alert = this.alertCtrl.create({
          title: 'Error Submitting Odometer',
          message: "We are unable to submit your odometer at this time.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {});

        console.log('error submitting odometer', err);
        // TODO: what is happening here, and how to better handle it?
      });
  }

  /**
   * Used as callback function to handle two cases:
   *  error = 'empty' - an empty odo
   *  error = 'invalid' - an invalid odo (not a whole number)
   *
   * @param error
   */
  handleOdometer(error): Promise<boolean> {
    // noinspection JSUnusedLocalSymbols
    return new Promise<boolean>((resolve, reject) => {
      const message = (error == 'empty' ? "Odometer is required. " : "") + "Odometer must be a positive whole number.";

      const alert = this.alertCtrl.create({
        title: 'Invalid Odometer',
        message: message,
        buttons: [
          {
            text: 'Update Ending Odometer',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      alert.present()
        .then(() => {});
    });
  }

  /**
   * Handler to verify that we meant the high odometer (compared to last reading) entered or allow to fix.
   *
   * @returns {Promise<boolean>}
   */
  handleHighMileage(): Promise<boolean> {
    // noinspection JSUnusedLocalSymbols
    return new Promise<boolean>((resolve, reject) => {
      let units = this.userInfo.country == 'US' ? 'miles' : 'kilometers';
      const message = "You have entered more than 500 " + units + " for the day. Do you want to change this?";

      const alert = this.alertCtrl.create({
        title: 'High Mileage Detected',
        message: message,
        buttons: [
          {
            text: 'Use Mileage Entered',
            handler: () => {
              console.log('using current mileage');
              resolve(true);
            }
          },
          {
            text: 'Update Ending Odometer',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      alert.present()
        .then(() => {});
    });
  }

  /**
   * Handler to verify that we meant the unusually high (1000000+) odometer entered or allow to fix.
   *
   * @returns {Promise<boolean>}
   */
  handleHighOdometer() {
    // noinspection JSUnusedLocalSymbols
    return new Promise<boolean>((resolve, reject) => {
      let units = this.userInfo.country == 'US' ? 'miles' : 'kilometers';
      const message = "You have entered an unusually high odometer. Please enter an odometer reading of less than 1 million " + units + ".";

      const alert = this.alertCtrl.create({
        title: 'High Odometer Detected',
        message: message,
        buttons: ['OK']
      });

      alert.present()
        .then(() => {});

      alert.onDidDismiss(() => {
        resolve();
      });
    });
  }

  /**
   * Verify that we meant the low odometer entered or allow to fix.
   * Low means that ending < start+business
   *
   * @returns {Promise<boolean>}
   */
  handleLowOdometer(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      const message = "Your ending odometer is less than the sum of business mileage for the day. Do you want to change this?";

      const alert = this.alertCtrl.create({
        title: 'Low Odometer Detected',
        message: message,
        buttons: [
          {
            text: 'Use Odometer Entered',
            handler: () => {
              console.log('using current odometer');
              resolve(true);
            }
          },
          {
            text: 'Update Ending Odometer',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      alert.present()
        .then(() => {});
    });
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Used to get sum of original and adjusted mileage for display purposes.
   *
   * @param num1
   * @param num2
   */
  addThese(num1: number, num2: number) {
    return parseFloat('' + num1) + parseFloat('' + num2);
  }

  /**
   * Use edit trip modal with currently selected trip.
   *
   * @param trip
   * @param index
   */
  onShowEditTrip(trip: Trip, index: number) {
    const modal = this.modalCtrl.create(EditTripModalPage, {
      trip: trip,
      mileage_label: this.userInfo.mileage_label,
      use_stop_contract: this.userInfo.use_stop_contract,
      use_tickets: this.use_tickets
    });
    modal.present()
      .then(() => {});

    modal.onDidDismiss(data => {
      console.log('modal returned ', data);
      this.business_mileage = data['business_mileage'];
    });
  }
}
