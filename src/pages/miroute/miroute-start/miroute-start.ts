import {Component, ViewChild} from '@angular/core';
import {AlertController, LoadingController, ModalController, NavController, Select} from "ionic-angular";
import {Location} from "../../../models/location";
import {Geolocation} from "@ionic-native/geolocation";
import {NgForm} from "@angular/forms";
import {MirouteDrivingPage} from "../miroute-driving/miroute-driving";
import {AuthService} from "../../../services/auth";
import {Stop} from "../../../models/stop";
import {MirouteNewStopPage} from "../miroute-new-stop/miroute-new-stop";
import {TabsPage} from "../../tabs/tabs";
import {ApiService} from "../../../services/api";
import {UserInfo} from "../../../models/userInfo";
import {MirouteStoppedPage} from "../miroute-stopped/miroute-stopped";
import {MirouteEndPage} from "../miroute-end/miroute-end";
import {MirouteDayEndedPage} from "../miroute-day-ended/miroute-day-ended";

declare var google;

@Component({
  selector: 'page-miroute-start',
  templateUrl: 'miroute-start.html',
})
export class MirouteStartPage {
  //region DATA MEMBERS
  id;                                               // id for this component, for debug purposes
  userInfo: UserInfo;                               // model instance, data from server
  currentLocation: Location = {lat: 0, lng: 0};     // for map without stop, show current location
  location: Location = {lat: 0, lng: 0};            // model attached to chosen location in select
  stops: Stop[] = [];                               // used to hold nearby stops
  stopType: string;                                 // chosen stop type (driver, company), sent to server
  stopName: string;                                 // chosen stop name, sent to server
  selected_contracts: number[] = [];                // all contracts associated to nearby stops, from server
  selected_tickets: number[] = [];                  // all tickets associated to nearby stops, from server
  use_stop_contract: number = 0;                    // company selection
  use_tickets: number = 0;                          // certain drivers in stop_contract companies
  got_stops_from_server = false;                    // used to disable button when request sent to server
  locationIsSet = false;                            // trigger to display the map
  showOtherContract = false;                        // used to show free-text other contract input
  stopIsSelected = false;                           // trigger to display stop on map
  stopIsSubmitted = false;                          // let HTML know if 'where' or 'stopped' state
  last_starting_odometer: number;                   // display helpful to drivers
  last_ending_odometer: number;                     // display helpful to drivers
  refreshSubscription;                              // attached to auth refreshDataEmitter subscription
  logoutSubscription;                               // attached to auth unsubscribeAllEmitter subscription
  @ViewChild('stop_select') stopSelect: Select;     // direct attach to html element
  //endregion

  /**
   * Constructor only sets id for this instance
   *
   * @param geo
   * @param loadingCtrl
   * @param navCtrl
   * @param alertCtrl
   * @param authService
   * @param modalCtrl
   * @param api
   */
  constructor(private geo: Geolocation,
              private loadingCtrl: LoadingController,
              private navCtrl: NavController,
              private alertCtrl: AlertController,
              private authService: AuthService,
              private modalCtrl: ModalController,
              private api: ApiService) {

    this.id = Math.random() * 10;
    console.log('\n\n------- CONSTRUCTING MIROUTE-START.TS -------');
    console.log('ID: ' + this.id);
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Call setup() every time we enter page,
   * subscribe to auth emitters
   */
  ionViewWillEnter() {
    this.setup()
      .then(() => {
        console.log('start: setup done');
      })
      .catch(err => {
        console.log('start: failed to setup', err);
      });

    console.log('MIROUTE-START.TS SUBSCRIBING');
    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      console.log('MIROUTE-START.TS ID ' + this.id + ' refreshing data');

      if (refresh) {
        this.setup()
          .then(() => {
            console.log('MIROUTE-START.TS ID ' + this.id + ' refresh then setup done');
          })
          .catch(err => {
            console.log('start: failed to refresh data', err);
          });
      }
    });

    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    })
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Unsubscribe to auth emitter, wipe out local data
   */
  ionViewWillLeave() {
    console.log('MIROUTE-START ID: ' + this.id + ' UNSUBSCRIBING \n\n');
    this.refreshSubscription.unsubscribe();
    this.userInfo = <UserInfo>{};
  }

  /**
   * Get user info from server. Set local variables.
   * Test if we are still on a valid page (we may have updated info online).
   * Get current location, and stops near that location.
   */
  setup() {

    return new Promise<any>((resolve, reject) => {
      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          // console.log('start: userInfo', userInfo);
          this.userInfo = userInfo;
          this.testPage()
            .then(() => {
              this.last_starting_odometer = this.userInfo.last_starting_odometer;
              this.last_ending_odometer = this.userInfo.last_ending_odometer;
              // console.log('start: last_ending_odometer', this.last_ending_odometer);
              this.use_stop_contract = this.userInfo.use_stop_contract;
              this.use_tickets = this.userInfo.use_tickets;

              /**
               * We need our current location to find nearby stops
               */
              this.findCurrentLocation()
                .then(() => {
                  this.getStopsFromServer()
                    .then(resolve)
                    .catch(err => {
                      reject(err);
                    });
                })
                .catch(err => {
                  reject(err);
                });
            })
            .catch(() => {
              // at this point page was incorrect and nav has moved away
            });
        })
        .catch(err => {
          reject(err);
        });
    });
  }

  /**
   * Page from api must be 'where' or 'stopped'
   * 'start_day' or 'end_previous_day' need to be forced
   */
  testPage() {
    console.log('start: testPage ' + this.userInfo.miroute_page);
    return new Promise<any>((resolve, reject) => {
      switch (this.userInfo.miroute_page) {
        case 'end_previous_day':
        case 'end_day':
        case 'end-day':
          this.navCtrl.setRoot(MirouteEndPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'day_ended':
          this.navCtrl.setRoot(MirouteDayEndedPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'driving':
          this.navCtrl.setRoot(MirouteDrivingPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'unknown':
          this.authService.logout()
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'where':
        case 'stopped':
          this.navCtrl.setRoot(MirouteStoppedPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
          reject();
          break;
        case 'start_day':
        default:
          resolve();
      }
    });
  }

  /**
   * Get stops close to driver's current location
   */
  getStopsFromServer() {
    // console.log('start: getting stops from server', this.location);
    return new Promise<any>((resolve, reject) => {
      this.authService.getNearbyStops(this.location.lat, this.location.lng)
        .then((stops: Stop[]) => {
          console.log('start: nearby stops', stops);
          this.stops = stops;
          this.got_stops_from_server = true;
          resolve();
        })
        .catch(err => {
          // console.log('start: error getting nearby stops', err);
          reject(err);
        });
    });
  }

  /**
   * Called from the pull-down refresh, rerun setup()
   *
   * @param refresher
   */
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.setup()
      .then(() => {
        console.log('finished refresh');
        refresher.complete();
      });
  }

  /**
   * Initial form submit. Determines if basic data is valid, then passes info to checkOdometerDiff()
   *
   * @param form
   */
  onSubmit(form: NgForm) {
    console.log('start form', form.value);

    let selectedStop = this.stops[this.stopSelect.value];
    let starting_odometer = form.value.starting_odometer;

    let business_purpose: string;
    if (this.use_stop_contract) {
      business_purpose = form.value.contract_number == 'Other' ? ''+form.value.other_contract : ''+form.value.contract_number;
    } else {
      business_purpose = form.value.business_purpose;
    }

    if (this.use_stop_contract && (business_purpose == null || business_purpose == '')) {
      const contractAlert = this.alertCtrl.create({
        title: "No Contract Provided",
        message: "Please select a contract from the drop-down list, or select \"Other\" and enter in an alternate contract in the text field provided.",
        buttons: ['OK']
      });
      contractAlert.present()
        .then(() => {});
      return;
    } else if (this.use_stop_contract && !this.use_tickets && (business_purpose.match(/[^\d]/) || business_purpose.length !== 4)) {
      const contractAlert = this.alertCtrl.create({
        title: "Not a Valid Contract Number",
        message: "Please provide a contract number of 4 numeric digits only.",
        buttons: ['OK']
      });
      contractAlert.present()
        .then(() => {});
      return;
    } else if (this.use_stop_contract && this.use_tickets && (business_purpose.match(/[^\d]/) || (business_purpose.length !== 6 && business_purpose.length !== 4))) {
      const contractAlert = this.alertCtrl.create({
        title: "Not a Valid Ticket or Contract Number",
        message: "Please provide a 6-digit ticket number or 4-digit contract number.",
        buttons: ['OK']
      });
      contractAlert.present()
        .then(() => {});
      return;
    }

    let continue_submit = true;
    console.log('here', starting_odometer, this.last_starting_odometer);

    if (this.last_starting_odometer != null && this.last_starting_odometer != starting_odometer) {
      const alert = this.alertCtrl.create({
        title: 'Start Odometer Changed',
        message: "You are changing the starting odometer from the value initially set in Fusion Online. Did you want to change your odometer?",
        buttons: [
          {
            text: 'Use ' + this.last_starting_odometer,
            handler: () => {
              starting_odometer = this.last_starting_odometer;
              continue_submit = true;
            }
          },
          {
            text: 'Use ' + starting_odometer,
            handler: () => {
              continue_submit = true;
            }
          },
          {
            text: 'Edit Value',
            handler: () => {
              continue_submit = false;
            }
          }
        ]
      });
      alert.present()
        .then(() => {});

      alert.onDidDismiss(() => {
        //console.log('after alert closed', continue_submit, starting_odometer);
        if (continue_submit) {
          this.checkOdometerDiff(starting_odometer, business_purpose, selectedStop.id);
        }
      });
    } else {
      this.checkOdometerDiff(starting_odometer, business_purpose, selectedStop.id);
    }
  }

  /**
   * Validator for odometer values.
   * If successful, call continueSubmit()
   * @param starting_odometer
   * @param business_purpose
   * @param stop_id
   */
  checkOdometerDiff(starting_odometer, business_purpose, stop_id) {
    if (this.last_ending_odometer != null) {
      const diff = starting_odometer - this.last_ending_odometer;

      if (starting_odometer > 999999) {
        this.handleUnusuallyHighOdometer()
          .then(() => {
            const input: any = document.querySelector('ion-input input[name=starting_odometer]');
            //console.log(input);
            setTimeout(() => {
              input.focus();
            }, 300);
          });
      } else if (starting_odometer < 0) {
        let alert = this.alertCtrl.create({
          title: 'Starting Odometer Less Than Zero',
          message: 'Please enter a non-negative starting odometer.',
          buttons: ['OK'],
        });
        alert.present()
          .then(() => {});
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input input[name=starting_odometer]');
          setTimeout(() => {
            input.focus();
          }, 300);
        })
      } else if (starting_odometer % 1 != 0) {
        let alert = this.alertCtrl.create({
          title: 'Starting Odometer Contains Decimal',
          message: 'Please enter a whole number starting odometer.',
          buttons: ['OK'],
        });
        alert.present()
          .then(() => {});
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input input[name=starting_odometer]');
          setTimeout(() => {
            input.focus();
          }, 300);
        })
      } else if (diff < 0) {
        this.handleLowOdometer()
          .then(result => {
            if (!result) {
              const input: any = document.querySelector('ion-input input[name=starting_odometer]');
              setTimeout(() => {
                input.focus();
              }, 300);
            } else {
              this.continueSubmit(starting_odometer, business_purpose, stop_id);
            }
          });
      } else if (diff > 1000) {
        this.handleHighOdometer()
          .then(result => {
            if (!result) {
              const input: any = document.querySelector('ion-input input[name=starting_odometer]');
              setTimeout(() => {
                input.focus();
              }, 300);
            } else {
              this.continueSubmit(starting_odometer, business_purpose, stop_id);
            }
          });
      } else if (business_purpose.length > 199) {
        let alert = this.alertCtrl.create({
          title: 'Business Purpose Too Long',
          message: "Please enter a business purpose of less than 200 characters.",
          buttons: ['OK']
        });
        alert.present()
          .then(() => {});
        alert.onDidDismiss(() => {
          const input: any = document.querySelector('ion-input input[name=business_purpose]');
          setTimeout(() => {
            input.focus();
          }, 300);
        });
      } else {
        this.continueSubmit(starting_odometer, business_purpose, stop_id);
      }
    } else {
      this.continueSubmit(starting_odometer, business_purpose, stop_id);
    }
  }

  /**
   * All validation passed, continue to submit starting odomter
   *
   * @param starting_odometer
   * @param business_purpose
   * @param stop_id
   */
  continueSubmit(starting_odometer, business_purpose, stop_id) {
    this.stopIsSubmitted = true;

    this.api.fetch('submitStartingOdometer', {
      user_id: this.userInfo.user_id,
      starting_odometer: starting_odometer,
      business_purpose: business_purpose,
      stop_id: stop_id,
      stop_type: this.stopType,
      stop_name: this.stopName
    }, true)
      .subscribe((userInfo: UserInfo) => {
          console.log('start: got new userInfo', userInfo);
          this.userInfo = userInfo;

          this.navCtrl.setRoot(MirouteDrivingPage)
            .then(() => {
              this.refreshSubscription.unsubscribe();
            });
        },
        err => {
          console.log('start: submit starting odo fail', err);
          this.stopIsSubmitted = false;
        }
      );

  }

  /**
   * Handler for an abnormally low odometer
   */
  handleLowOdometer() {
    // noinspection JSUnusedLocalSymbols
    return new Promise<boolean>((resolve, reject) => {
      const message = "Your starting odometer is less than your last ending odometer of " + this.last_ending_odometer + ". Do you want to change this?";

      const alert = this.alertCtrl.create({
        title: 'Low Odometer Detected',
        message: message,
        buttons: [
          {
            text: 'Use Odometer Entered',
            handler: () => {
              console.log('using current odometer');
              resolve(true);
            }
          },
          {
            text: 'Update Starting Odometer',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      alert.present()
        .then(() => {});
    });
  }

  /**
   * Handler for an unexpected high odometer (1000+ more than last reading)
   */
  handleHighOdometer() {
    // noinspection JSUnusedLocalSymbols
    return new Promise<boolean>((resolve, reject) => {
      const message = "Your starting odometer is more than 1000 greater than your last ending odometer of " + this.last_ending_odometer + ". Do you want to change this?";

      const alert = this.alertCtrl.create({
        title: 'High Odometer Detected',
        message: message,
        buttons: [
          {
            text: 'Use Odometer Entered',
            handler: () => {
              console.log('using current odometer');
              resolve(true);
            }
          },
          {
            text: 'Update Starting Odometer',
            handler: () => {
              resolve(false);
            }
          }
        ]
      });

      alert.present()
        .then(() => {});
    });
  }

  /**
   * Handler for unusually high odometer (1000000+)
   */
  handleUnusuallyHighOdometer() {
    // noinspection JSUnusedLocalSymbols
    return new Promise<boolean>((resolve, reject) => {
      let units = this.userInfo.country == 'US' ? 'miles' : 'kilometers';
      const message = "You have entered an unusually high odometer. Please enter an odometer reading of less than 1 million " + units + ".";

      const alert = this.alertCtrl.create({
        title: 'High Odometer Detected',
        message: message,
        buttons: ['OK']
      });

      alert.present()
        .then(() => {});
      alert.onDidDismiss(() => {
        resolve();
      });
    });
  }

  /**
   * Set current location
   */
  findCurrentLocation() {
    return new Promise((resolve, reject) => {
      const loading = this.loadingCtrl.create({
        content: 'Finding location...'
      });
      loading.present()
        .then(() => {});

      let options = {
        enableHighAccuracy: true,
        timeout: 5000,
        maximumAge: 0
      };

      this.geo.getCurrentPosition(options)
        .then(location => {
          loading.dismiss()
            .then(() => {});
          //console.log('location', location);

          // set both currentLocation and location
          this.currentLocation.lat = location.coords.latitude;
          this.currentLocation.lng = location.coords.longitude;

          this.location.lat = location.coords.latitude;
          this.location.lng = location.coords.longitude;
          //console.log(this.location);
          this.locationIsSet = true;
          resolve();
        })
        .catch(err => {
          loading.dismiss()
            .then(() => {});
          console.log(err);
          reject();
        });
    });
  }

  /**
   * When stop is selected, set location to that stop lat&lng, get contracts or tickets if necessary.
   *
   * @param event
   */
  onSelectStop(event) {
    //console.log('changed', event);
    if (event == 'new') {
      this.stopIsSelected = false;
      this.createNewStop();
    } else if (event >= 0) {
      this.locationIsSet = false;
      this.location = {lat: this.stops[event]['latitude'], lng: this.stops[event]['longitude']};
      this.stopType = this.stops[event]['company_stop_id'] == null ? 'driver' : 'company';
      this.stopName = this.stops[event]['name'];
      if (this.use_stop_contract) {
        this.selected_contracts = this.stops[event]['contracts'];
      }
      if (this.use_tickets) {
        this.selected_tickets = this.stops[event]['tickets'];
      }
      this.locationIsSet = true;
      this.stopIsSelected = true;
    } else if (event == -1) {
      this.stopIsSelected = false;
    } else {
      this.stopIsSelected = false;
    }
  }

  /**
   * Use new stop modal to enter a new driver stop.
   * Stop will be created upon closing modal, even if we do not use
   * this location now.
   */
  createNewStop() {
    let me = this;
    //console.log('creating a new stop', this.currentLocation);
    let google_maps_geocoder = new google.maps.Geocoder();
    const latlng = new google.maps.LatLng(this.currentLocation.lat, this.currentLocation.lng);

    //console.log('geocoding now', latlng);
    google_maps_geocoder.geocode(
      {location: latlng},
      function (results, status) {
        //console.log(results, status);
        if (status != 'OK') {
          me.handleBadGeocodingResponse(status);
        } else {
          let newStopModal = me.modalCtrl.create(MirouteNewStopPage, {
            geo: results[0],
            latitude: me.currentLocation.lat,
            longitude: me.currentLocation.lng,
            user_id: me.userInfo.user_id
          });
          //console.log('presenting modal', newStopModal);
          newStopModal.present()
            .then(() => {});

          newStopModal.onDidDismiss((data: any) => me.modalDismissed(data));
        }
      }
    );
  }

  /**
   * Set newly created stop as current location.
   * Still may change location before we submit.
   *
   * @param data
   */
  modalDismissed(data: any) {
    //console.log('modal dismissed', data, this.stopSelect);
    if (data['newStop']) {
      this.stops.push(data['newStop']);
      this.stopSelect.setValue(this.stops.length - 1);
      this.stopType = 'driver';
      this.stopName = data['newStop']['name'];
    }
    else {
      this.stopSelect.setValue(-1);
    }
  }

  /**
   * No state of geocoding error has an implementation.
   * TODO: add alerts for each state
   * @param status
   */
  handleBadGeocodingResponse(status) {
    switch (status) {
      case 'ZERO_RESULTS':
        /**
         * indicates that the geocode was successful but returned no results. This may occur if the geocoder was passed a non-existent address
         */
        break;
      case 'OVER_DAILY_LIMIT':
        /**
         * indicates any of the following
         * - api key missing or invalid
         * - billing not enabled for account
         * - self-imposed usage cap has been exceeded
         * - provided method of payment is no longer valid
         */
        break;
      case 'OVER_QUERY_LIMIT':
        /**
         * indicates that you are over your quota
         */
        break;
      case 'REQUEST_DENIED':
        /**
         * indicates that your request was denied
         */
        break;
      case 'INVALID_REQUEST':
        /**
         * generally indicates that the query (address, components or latlng) is missing
         */
        break;
      case 'UNKNOWN_ERROR':
        /**
         * indicates that the request could not be processed due to a server error. The request may succeed if you try again
         */
        break;
      default:
        break;
    }

    const alert = this.alertCtrl.create({
      title: 'Unable To Create New Location',
      message: "We were unable to create a new location at this time. Please try again shortly, or contact your CSA if the problem persists.",
      buttons: ['OK']
    });
    alert.present()
      .then(() => {});

    alert.onDidDismiss(() => {
      this.navCtrl.setRoot(TabsPage)
        .then(() => {
          this.refreshSubscription.unsubscribe();
        });
    });
  }

  onSelectContract(event: any) {
    this.showOtherContract = event == 'Other';
  }
}
