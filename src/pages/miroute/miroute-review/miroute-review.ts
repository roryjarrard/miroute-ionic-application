import {Component} from '@angular/core';
import {Trip} from "../../../models/trip";
import {AuthService} from "../../../services/auth";
import {UserInfo} from "../../../models/userInfo";
import {AlertController, ModalController, NavController} from "ionic-angular";
import {EditTripModalPage} from "../../partials/edit-trip-modal/edit-trip-modal";
import moment from "moment";

@Component({
  selector: 'page-miroute-review',
  templateUrl: 'miroute-review.html',
})
export class MirouteReviewPage {
  //region DATA MEMBERS
  userInfo: UserInfo;                         // model for user data
  trips: Trip[] = [];                         // trips to show route driven today
  last_entry_date: string;                    // used for date calendar display
  last_entry_day: number;                     // used for date calendar display
  last_entry_month: string;                   // used for date calendar display
  last_entry_year: number;                    // used for date calendar display
  last_starting_odometer: number;             // for display
  last_ending_odometer: number;               // for display
  business_mileage: number = 0;               // sum for display
  page: string = '';                          // page you are at (review isn't set in state)
  display_route_map: boolean = false;         // use partial to display route for today's stops
  display_agm_map: boolean = false;           // agm map shows one stop better (not route)
  display_current_location: boolean = false;  // not used, currently only show location when at an actual stop
  shownGroup;                                 // used to show content when a stop is toggled
  showAddTrips = false;                       // can resume driving after ending day, only in Fusion
  edit_mode = false;                          // display toggle, opens the edit trip modal
  can_edit_mileage = false;                   // if day was in the past, you may not edit
  day_to_end: string;                         // show specific day when in the past
  refreshSubscription;                        // attached to auth refreshDataEmitter subscription
  logoutSubscription;                         // attached to auth unsubscribeAllEmitter subscription
  full_mileage_units_name: string = '';       // used to display mileage or kilometers based on country
  use_stop_contract: number = 0;              // company selection
  use_tickets: number = 0;                    // certain drivers in stop_contract companies
  //endregion

  /**
   * Constructor does nothing
   *
   * @param authService
   * @param alertCtrl
   * @param modalCtrl
   * @param navCtrl
   */
  constructor(private authService: AuthService,
              private alertCtrl: AlertController,
              private modalCtrl: ModalController,
              private navCtrl: NavController) {
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Call setup(), subscribe to auth emitters.
   */
  ionViewWillEnter() {
    this.setup()
      .then(() => {
        console.log('review: setup done');
      })
      .catch(err => {
        console.log('review: failed to setup', err);
      });

    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      console.log('review: refreshing data');
      if (refresh) {
        this.setup()
          .then(() => {
            console.log('review: refresh then setup done');
          })
          .catch(err => {
            console.log('review: failed to refresh data', err);
          });
      }
    });
    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    })
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Unsubscribe from auth emitters, wipe out local user info
   */
  ionViewWillLeave() {
    console.log('MIROUTE-REVIEW UNSUBSCRIBING');
    this.refreshSubscription.unsubscribe();
    this.userInfo = <UserInfo>{};
  }

  /**
   * Retrieve current user info from server. Test if need to end previous day.
   * Set local values. Show route map if more than 1 stop, agm map if only 1 stop, no map if no stops.
   */
  setup() {
    return new Promise<any>((resolve, reject) => {

      // Commenting out loader for now, weird because map also has loader
      //const loader = this.loadingCtrl.create();
      //loader.present();

      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('review: userInfo', userInfo);
          this.userInfo = userInfo;

          //console.log('specifically business mileage is ' + userInfo.lastDrivingRecord.business_mileage);
          this.last_entry_date = userInfo.last_entry_date;
          this.last_entry_day = moment(this.last_entry_date).date();
          this.last_entry_month = moment(this.last_entry_date).format('MMM');
          this.last_entry_year = moment(this.last_entry_date).year() < moment().year() ? moment(this.last_entry_date).year() : null;
          this.last_starting_odometer = userInfo.lastDrivingRecord['starting_odometer'];
          this.last_ending_odometer = userInfo.lastDrivingRecord['ending_odometer'];
          // this.last_ending_odometer = userInfo.last_ending_odometer;
          this.business_mileage = userInfo.lastDrivingRecord.business_mileage ? userInfo.lastDrivingRecord.business_mileage : 0;

          this.use_stop_contract = this.userInfo.use_stop_contract;
          this.use_tickets = this.userInfo.use_tickets;

          this.full_mileage_units_name = this.userInfo.country == 'US' ? 'Miles' : 'Kilometers';

          this.page = userInfo.miroute_page;
          this.trips = userInfo.trips;
          this.day_to_end = moment(this.userInfo.last_entry_date).format('dddd, MMMM Do YYYY');

          this.can_edit_mileage = moment().format('YYYY-MM-DD') === moment(this.userInfo.lastDrivingRecord.trip_date).format('YYYY-MM-DD');

          if (this.trips && this.trips.length > 1) {
            this.display_route_map = true;
            this.display_agm_map = false;
            this.display_current_location = false;
            //loader.dismiss();
          } else if (this.trips && this.trips.length == 1) {
            this.display_agm_map = true;
            this.display_route_map = false;
            this.display_current_location = false;
            //loader.dismiss();
          } else {
            //loader.dismiss();
            // find and display current location
            this.showCurrentLocation()
              .then(() => {
                console.log('showing current location');
              });
          }

          if (this.page == 'day_ended') {
            this.showAddTrips = true;
          }

          if (userInfo.miroute_page == 'end_previous_day') {
            this.endPreviousDay();
          }

          resolve();
        })
        .catch(err => {
          console.log('review: could not fetch userInfo');
          reject(err);
        });
    });
  }

  /**
   * Pull-down refresh method, calls setup()
   *
   * @param refresher
   */
  doRefresh(refresher) {
    console.log('Begin async operation', refresher);

    this.setup()
      .then(() => {
        console.log('finished refresh');
        refresher.complete();
      });
  }

  /**
   * If you need to end previous day, show modal, then force location to end day page.
   */
  endPreviousDay() {
    const alert = this.alertCtrl.create({
      title: 'End Your Previous Day',
      message: "Please end your day from " + this.day_to_end,
      buttons: ['OK']
    });
    alert.present()
      .then(() => {});

    alert.onDidDismiss(() => {
      this.refreshSubscription.unsubscribe();
      setTimeout(() => {
        this.navCtrl.parent.select(1);
      }, 300);
    });
  }

  /**
   * Toggle which trip is being shown for display and edit purposes
   *
   * @param group
   */
  toggleGroup(group) {
    if (this.isShownGroup(group)) {
      this.shownGroup = null;
    } else {
      this.shownGroup = group;
    }
  }

  /**
   * If this group is shown used to display elements in the html
   *
   * @param group
   */
  isShownGroup(group) {
    return this.shownGroup == group;
  }

  // noinspection JSUnusedGlobalSymbols
  /**
   * Resume driving if you already ended your day.
   * Only works for Fusion.
   */
  onAddTrips() {
    const alert = this.alertCtrl.create({
      title: 'Add Additional Trips',
      message: "Do you want to continue driving for the current day?",
      buttons: [
        {
          text: 'Resume Driving',
          handler: () => {
            this.authService.resumeDriving(this.last_entry_date)
              .then(response => {
                this.userInfo = response['userInfo'];
                console.log('userinfo AFTER resume', response['userInfo']);
                this.authService.updateAuth(true);
              });
          }
        },
        {
          text: 'Cancel',
          handler: () => {

          }
        }
      ]
    });
    alert.present()
      .then(() => {});
    /*alert.onDidDismiss(() => {});*/
  }

  /**
   * Set booleans to show the current map location.
   * Deprecated, only show map if trips have been logged for the day
   */
  showCurrentLocation() {
    // noinspection JSUnusedLocalSymbols
    return new Promise<any>((resolve, reject) => {

      this.display_current_location = true;
      this.display_route_map = false;
      this.display_agm_map = false;
      resolve();
    });
  }

  /**
   * Open the edit trip modal with the currently selected stop to be edited
   *
   * @param trip
   */
  onShowEditTrip(trip: Trip) {
    const modal = this.modalCtrl.create(EditTripModalPage, {
      trip: trip,
      mileage_label: this.userInfo.mileage_label,
      use_stop_contract: this.userInfo.use_stop_contract,
      use_tickets: this.use_tickets
    });
    modal.present()
      .then(() => {});

    modal.onDidDismiss(data => {
      console.log('modal returned ', data);
      if (data['business_mileage'] !== null) {
        this.business_mileage = data['business_mileage'];
      }
    });
  }

  // noinspection JSMethodCanBeStatic
  /**
   * Computed value used to show sum of original and adjusted mileage in html
   *
   * @param num1
   * @param num2
   */
  addThese(num1: number, num2: number) {
    return parseFloat('' + num1) + parseFloat('' + num2);
  }
}
