import {Component} from '@angular/core';
import {NavController} from "ionic-angular";
import {MirouteStoppedPage} from "../miroute-stopped/miroute-stopped";
import {UserInfo} from "../../../models/userInfo";
import {MirouteEndPage} from "../miroute-end/miroute-end";
import {StorageService} from "../../../services/storage";
import {Trip} from "../../../models/trip";
import {AuthService} from "../../../services/auth";
import {MirouteStartPage} from "../miroute-start/miroute-start";
import {MirouteDayEndedPage} from "../miroute-day-ended/miroute-day-ended";

@Component({
  selector: 'page-miroute-driving',
  templateUrl: 'miroute-driving.html',
})
export class MirouteDrivingPage {
  last_stop_name: string = '';
  disable_arrive_button: boolean = false;
  user_id: number;
  userInfo: UserInfo;
  id;
  refreshSubscription;
  logoutSubscription;

  constructor(private storageService: StorageService,
              private navCtrl: NavController,
              private authService: AuthService) {
    this.id = Math.random() * 10;

    console.log('\n\n------- CONSTRUCTING MIROUTE-DRIVING.TS -------');
    console.log('ID: ' + this.id);

  }

  ionViewWillEnter() {
    console.log('\n\nION VIEW WILL ENTER MIROUTE-DRIVING.TS');
    console.log('driving calling setup');
    this.setup()
      .then(() => {
        console.log('driving: setup done');
      })
      .catch(err => {
        console.log('driving: failed to setup', err);
      });

    console.log('MIROUTE-DRIVING.TS subscribing');
    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      console.log('MIROUTE-DRIVING.TS ID:' + this.id + ' refreshing data');

      if (refresh) {
        this.setup()
          .then(() => {
            console.log('MIROUTE-DRIVING.TS ID:' + this.id + 'refresh then setup done');
          })
          .catch(err => {
            console.log('driving: failed to refresh data', err);
          });
      }
    });

    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    })
  }

  ionViewWillLeave() {
    console.log('MIROUTE-DRIVING ID: ' + this.id + ' UNSUBSCRIBING \n\n');
    this.refreshSubscription.unsubscribe();
    this.userInfo = <UserInfo>{};
  }

  setup() {
    return new Promise<any>((resolve, reject) => {
      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('driving: getUserInfo', userInfo);
          this.userInfo = userInfo;
          this.testPage()
            .then(() => {
              this.user_id = this.userInfo.user_id;
              //console.log('driving: trips before lastStop', this.userInfo.trips);
              const lastStop: Trip = this.userInfo.trips.slice(-1)[0];
              //console.log('driving: last stop', lastStop);
              this.storageService.updateUserInfo({lastStop: lastStop, last_stop_name: lastStop.stop.name})
                .then(() => {
                  this.last_stop_name = lastStop.stop.name;
                });
              resolve();
            })
            .catch(() => {
              // this means we should have been on another page, and nav has pushed that page already
            });

        })
        .catch(err => {
          console.log('driving: unable to get userInfo from auth');
          reject(err);
        });
    });
  }

  /**
   * Page from api must be 'driving'
   * 'start_day' or 'end_previous_day' need to be forced
   */
  testPage() {
    console.log('driving: testPage ' + this.userInfo.miroute_page);
    return new Promise<any>((resolve, reject) => {
      switch (this.userInfo.miroute_page) {
        case 'start_day':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteStartPage);
          reject();
          break;
        case 'end_previous_day':
        case 'end_day':
        case 'end-day':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteEndPage);
          reject();
          break;
        case 'day_ended':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteDayEndedPage);
          reject();
          break;
        case 'where':
        case 'stopped':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteStoppedPage);
          reject();
          break;
        case 'unknown':
          this.authService.logout();
          reject();
          break;
        case 'driving':
        default:
          resolve();
      }
    });
  }


  // let the server know we have arrived
  onArrive() {
    // disable the button for visual indicator
    this.disable_arrive_button = true;

    // this will actually resync all userInfo
    this.authService.updateLastState({
      user_id: this.user_id,
      last_state: 'where'
    })
      .then((userInfo: UserInfo) => {
        this.storageService.set('userInfo', userInfo)
          .then(() => {
            console.log('miroute page was set in storage, redirecting');
            this.refreshSubscription.unsubscribe();
            this.navCtrl.setRoot(MirouteStoppedPage);
          });
      })
      .catch(err => {
        console.log('driving: unable to resolve update to last state', err);
        this.refreshSubscription.unsubscribe();
        this.navCtrl.setRoot(MirouteStoppedPage);
      });
  }

  onEndDay() {
    console.log('Ending Day');
    this.refreshSubscription.unsubscribe();
    this.navCtrl.push(MirouteEndPage);
  }
}
