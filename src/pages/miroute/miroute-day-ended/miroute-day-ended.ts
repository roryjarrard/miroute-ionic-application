import { Component } from '@angular/core';
import {NavController} from "ionic-angular";
import {AuthService} from "../../../services/auth";
import {UserInfo} from "../../../models/userInfo";
import {MirouteStartPage} from "../miroute-start/miroute-start";
import {MirouteEndPage} from "../miroute-end/miroute-end";
import {MirouteDrivingPage} from "../miroute-driving/miroute-driving";
import {MirouteStoppedPage} from "../miroute-stopped/miroute-stopped";
import moment from 'moment';
import {Trip} from "../../../models/trip";

@Component({
  selector: 'page-miroute-day-ended',
  templateUrl: 'miroute-day-ended.html',
})
export class MirouteDayEndedPage {
  userInfo: UserInfo;
  refreshSubscription;
  logoutSubscription;
  next_day: string;
  last_starting_odometer: number;
  last_ending_odometer: number;
  trips: Trip[];
  last_entry_date: string;
  last_entry_day: number;
  last_entry_month: string;
  full_mileage_units_name: string = '';
  business_mileage: number = 0;

  constructor(private navCtrl: NavController, private authService: AuthService) {
  }

  ionViewWillEnter() {
    this.setup()
      .then(() => {
        console.log('ended: setup done');
      })
      .catch(err => {
        console.log('ended: failed to setup', err);
      });

    this.refreshSubscription = this.authService.refreshDataEmitter.subscribe((refresh: boolean) => {
      if (refresh) {
        this.setup()
          .then(() => {
            console.log('ended: refresh then setup done');
          })
          .catch(err => {
            console.log('ended: failed to refresh data', err);
          });
      }
    });

    this.logoutSubscription = this.authService.unsubscribeAllEmitter.subscribe(() => {
      this.refreshSubscription.unsubscribe();
      delete this.logoutSubscription;
    })
  }

  ionViewWillLeave() {
    this.refreshSubscription.unsubscribe();
  }

  // all setup needs to do is detect a new day
  setup() {
    return new Promise<any>((resolve, reject) => {
      this.authService.fetchUserInfo()
        .then((userInfo: UserInfo) => {
          console.log('ended: getUserInfo', userInfo);
          this.userInfo = userInfo;
          this.last_starting_odometer = userInfo.lastDrivingRecord.starting_odometer;
          this.last_ending_odometer = userInfo.lastDrivingRecord.ending_odometer;
          this.trips = userInfo.trips;
          this.full_mileage_units_name = userInfo.country == 'US' ? 'Miles' : 'Kilometers';
          this.business_mileage = userInfo.lastDrivingRecord.business_mileage ? userInfo.lastDrivingRecord.business_mileage : 0;
          this.last_entry_date = userInfo.last_entry_date;
          this.last_entry_day = moment(this.last_entry_date).date();
          this.last_entry_month = moment(this.last_entry_date).format('MMM');
          this.next_day = moment().add(1, 'day').format('dddd, MMMM Do YYYY');
          this.testPage()
            .then(() => {
              // so much empty
              resolve();
            })
            .catch(() => {
              // this means we should have been on another page, and nav has pushed that page already
            });

        })
        .catch(err => {
          console.log('ended: unable to get userInfo from auth');
          reject(err);
        });
    });
  }

  /**
   * Page from api must be 'day_ended'
   * else we move away
   */
  testPage() {
    console.log('ended: testPage ' + this.userInfo.miroute_page);
    return new Promise<any>((resolve, reject) => {
      switch (this.userInfo.miroute_page) {
        case 'start_day':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteStartPage);
          reject();
          break;
        case 'end_previous_day':
        case 'end_day':
        case 'end-day':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteEndPage);
          reject();
          break;
        case 'driving':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteDrivingPage);
          reject();
          break;
        case 'where':
        case 'stopped':
          this.refreshSubscription.unsubscribe();
          this.navCtrl.setRoot(MirouteStoppedPage);
          reject();
          break;
        case 'unknown':
          this.authService.logout();
          reject();
          break;
        case 'day_ended':
        default:
          resolve();
      }
    });
  }

}
