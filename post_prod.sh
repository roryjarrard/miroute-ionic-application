#!/usr/bin/env bash
DATE=$(date +%F.%H.%M)
RELEASE="miroute.prod.$DATE"
MPATH='/var/www/vhosts/miroute'
MFILE='www.prod.tgz'
OWNER='rjarrard:apache'

sh -c "sudo mkdir -p $MPATH/releases/$RELEASE"
sh -c "sudo ln -sTf $RELEASE $MPATH/releases/latest"
sh -c "sudo ln -sTf $MPATH/releases/latest my_latest"
sh -c "sudo mv $MFILE my_latest/"
sh -c "cd my_latest && sudo tar -zxvf $MFILE"
sh -c "cd my_latest && sudo rm $MFILE"
sh -c 'cd my_latest && sudo find . -type d -exec chmod 775 {} \;'
sh -c 'cd my_latest && sudo find . -type f -exec chmod 664 {} \;'
sh -c "cd $MPATH/releases && sudo ln -sTf \$(readlink current) previous"
sh -c "cd $MPATH/releases && sudo ln -sTf \$(readlink latest) current"
sh -c "cd $MPATH/releases && sudo chown -R $OWNER ."
sh -c "cd $MPATH/releases && rm latest"
rm my_latest
sudo service httpd restart
exit
