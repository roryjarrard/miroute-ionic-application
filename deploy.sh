#!/bin/bash
### command line arg ENVIRONMENT in stage5, staging, prod
ENVIRONMENT=$1
SITE="www.$ENVIRONMENT.tgz"
if [ "$ENVIRONMENT" = 'prod' ]; then
    SSH_USER='cdprod'
else
    SSH_USER='cdstaging'
fi
POST_SCRIPT="post_$ENVIRONMENT.sh"

rm -Rf www/*
cross-env ENV=${ENVIRONMENT} ionic build --prod
cd www
tar -zcvf ${SITE} *
scp ${SITE} ${SSH_USER}:.
rm ${SITE}
cd ..
ssh -tt ${SSH_USER} 'bash -s' < ${POST_SCRIPT}
