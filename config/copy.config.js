var path = require('path');
var useDefaultConfig = require('@ionic/app-scripts/config/copy.config.js');

module.exports = function () {
  useDefaultConfig.copyFontawesomeFonts = {
    src: ['{{ROOT}}/node_modules/@fortawesome/fontawesome-pro/webfonts/*'],
    dest: '{{WWW}}/assets/webfonts'
  };

  useDefaultConfig.copyOtherFonts = {
    src: ['{{ROOT}}/node_modules/ionic-angular/fonts/*'],
    dest: '{{WWW}}/assets/fonts'
  };

  useDefaultConfig.copyFontawesomeCss = {
    src: ['{{ROOT}}/node_modules/@fortawesome/fontawesome-pro/css/all.css'],
    dest: '{{WWW}}/assets/css'
  };

  return useDefaultConfig;
};
