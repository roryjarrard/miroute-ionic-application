#!/bin/bash
### command line arg PLATFORM in stage5, staging, prod
ENVIRONMENT=$1
### command line arg PLATFORM in android, ios
PLATFORM=$2

rm -Rf www/*
cross-env ENV=${ENVIRONMENT} ionic cordova build ${PLATFORM} --prod
